# Thesis

This repository belongs to my PhD thesis.

## Set-Up

You need to download the following resources:
- MONAPipe v3.2 from [here](https://gitlab.gwdg.de/mona/pipy-public/-/releases/v3.2)
- MONACO v4.1 from [here](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v4.1)
- MONACO v5.0 from [here](https://gitlab.gwdg.de/mona/korpus-public/-/releases/v5.0)
- GER_SET from [here](https://doi.org/10.11588/data/BBQYD0)

Unzip the directories and place `pipy-public-v3.2`, `korpus-public-v4.1`, `korpus-public-v5.0` and `SitEnt_DE` in the same directory as `thesis`.

Follow the set-up instructions for MONAPipe (see [README](https://gitlab.gwdg.de/mona/pipy-public/-/blob/dc27cce2e460bb232a244062c5b0bf156c9c6411/README.md)) and make sure that you moved into its virtual environment.

Update the `flair` version:

```sh
pip install flair==0.11.3
```

Move into `thesis` (set `thesis` as working directory):

```sh
cd thesis
```

## Directory Structure

`paths.py` contains the paths to the external resources.

`texts.py` contains the name and number of annotated sentences for each text in MONACO v4.1 (`TEXTS_SENTS`) and MONACO v5.0 (`TEXTS_SENTS_2`) that has been used for statistical analyses or machine learning. The texts that have not been used are commented out.

The contents of the folders `agreements`, `analysis`, `deep_learning`, `key_referents` and `statistics` are described below.

## Inter-Annotator Agreements (`agreements`)

Move into the directory:

```sh
cd agreements
```

The script `agreements.py` computes inter-annotator agreements for Comment, GI, NfR and Attribution and writes them to according CSV files. To reproduce the results, delete the `.csv` files and run:

```sh
python agreements.py
```

## Annotations of Key Referents (`key_referents`)

The annotation collection `RRR.xml` contains annotations of key referents for all covertly quantified generalising passages (all BARE passages and many NEG passages) in a text. These additional annotations are made for all texts from MONACO 4.1 and some texts from later MONACO versions, and they are used for the statistical analyses. More details on key referents and the annotation guidelines can be found in the thesis.

## Statistical Analyses (`statistics`)

Move into the directory:

```sh
cd statistics
```

The script `create_data.py` performs the feature extraction and saves the samples line-wise in `data.csv` and column-wise in `data_transposed.csv`. The script calls `nlp.py`, which provides the NLP pipeline using MONAPipe, and `q_parsing.py`, which contains an implementation of the Q-parsing algorithm from the thesis. To recreate the data, delete the `.csv` files and run:

```sh
python create_data.py
```

The script `correlations.py` calculates most of the corpus statistics from the thesis. The script calls `measures.py`, which implements all the mathematical methods needed for the statistical tests. You can give one or several arguments to determine what to calculate. The results can also be found in according TXT files (already in LaTeX format for the thesis), which were created by running:

```sh
# number and length of GI passages per text
python correlations.py gi_per_text > correlations_gi_per_text.txt

# number and length of GI passages per tag
python correlations.py gi_per_tag > correlations_gi_per_tag.txt

# frequency of quantifiers per tag
python correlations.py q_per_tag > correlations_q_per_tag.txt

# correlations of GI and features
python correlations.py gi_vs_f > correlations_gi_vs_f.txt
```

For the correlations (`gi_vs_f`), the cooccurrences of features and tags are saved to `pickles` and loaded from there if existing. If you want to recount these, you have to clear the folder before calculating the correlations.

The script `overlaps.py` calculates the statistical tests for GI and the other phenomena. To recalculate the results, you have to run:

```sh
python overlaps.py > overlaps.txt
```

## Deep Learning (`deep_learning`)

Move into the directory:

```sh
cd deep_learning
```

The SH scripts perform all tagger evaluations for the thesis:

```sh
# evaluation of all taggers on Wieland
sh results_eval.sh

# evaluation of all taggers on Seghers
sh results_eval2.sh

# cross-validation of the flair tagger
sh results_cv.sh

# in-domain evaluation of all taggers on MONACO v5.0
sh results_in.sh

# off-domain evaluation of all taggers on GER_SET
sh results_off.sh
```

The scripts call `tagger_evaluation.py`, which evaluates a given tagger on a given evaluation set. The taggers are imported from MONAPipe, except for the `flair` taggers which are trained with `tagger.py` and saved to `models`. If you want to retrain them, you have to clear the folder first.

The output of `tagger_evaluation.py` is written to `results` and loaded from there if existing. If you want to recompute the results, you have to clear the folder first. **Note:** The results from `results_eval2.sh` (files for `Seghers`) cannot be reproduced, since the plain text is in the public domain and therefore not included in MONACO.

The scripts `imbalanced_sampler.py`, `lamb.py` and `sitent_reader.py` contain utility methods; see the comments in the respective file.

The script `add_up_confusion_matrices.py` creates combined confusion matrices for the texts in MONACO v5.0. It is necessary to run `results_in.sh` before this script. Run the script as:

```sh
python add_up_confusion_matrices.py
```

## Model Analysis (`analysis`)

Move into the directory:

```sh
cd analysis
```

The script `create_prediction_tables.py` creates the files `Bürger.csv` and `Wieland.csv`, which contain the gold passages and predicted passages from the `neural` tagger and the `flair` tagger for the respective text. Run the script as:

```sh
python create_prediction_tables.py
```

The files were copied to `Bürger_edited.csv` and `Wieland_edited.csv`, and then annotated for the analyses in the thesis. The files have the following columns:

- **sent:** sentence ID
- **clause:** clause ID
- **text:** clause text
- **gold:** gold annotation
- **neural:** prediction of the `neural` tagger
- **flair:** prediction of the `flair` tagger
- **gold-coherent:** clause type of the gold passage (for coherence analysis)
- **neural-coherent:** clause type of the passage predicted by the `neural` tagger (for coherence analysis)
- **flair-coherent:** clause type of the passage predicted by the `flair` tagger (for coherence analysis)
- **gi:** re-annotation (for adjusted F1 scores), using the tags `"Y"` (generalising), `"N"` (non-generalising) and `"X"` (ambiguous)
- **note:** `"ambig"` if the passage is ambiguous in the gold standard; otherwise empty

The results for the coherence analysis and the adjusted F1 scores can be computed by running:

```sh
python compute_scores.py
```
