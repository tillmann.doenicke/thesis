import csv
import numpy as np
import os
import pandas as pd
import spacy
import sys
from collections import OrderedDict

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from paths import PATH_TO_MONACO, PATH_TO_MONAPIPE
from texts import TEXTS_SENTS

sys.path.append(PATH_TO_MONAPIPE)
from agreement.agreement import return_fleiss_kappa, return_krippendorff_alpha, return_mathet_gamma
from evaluation.measures import compare_gold_pred_tokens, compare_gold_pred_clauses, tagwise_precisions, tagwise_recalls, tagwise_fscores
from pipeline.annotation import AnnotationList
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.slicer import max_sent_slicer
from pipeline.global_constants import SPACY_MAX_LENGTH
from pipeline.utils_methods import transfer_labels_to_passages
from settings import PARSING_PATH, PICKLE_PATH


# construct the pipeline to read the texts and annotations

model = os.path.join(PARSING_PATH, "de_ud_lg")

NLP = spacy.load(model, disable=["ner"])
NLP.max_length = SPACY_MAX_LENGTH

def pw(doc, func, **kwargs):
    return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)

pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=700)
NLP.add_pipe(pickle_init_, name="pickle_init", before="tagger")

dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
NLP.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

lemma_fixer_ = lambda doc: pw(doc, lemma_fixer)
NLP.add_pipe(lemma_fixer_, name="lemma_fixer", after="tagger")

spacy_sentencizer_ = lambda doc: pw(doc, spacy_sentencizer)
NLP.add_pipe(spacy_sentencizer_, name="sentencizer", before="parser")

max_sent_slicer_ = lambda doc: pw(doc, max_sent_slicer)
NLP.add_pipe(max_sent_slicer_, name="slicer", after="parser")

dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
NLP.add_pipe(dependency_clausizer_, name="clausizer")

annotation_reader_catma_ = lambda doc: annotation_reader_catma(doc, corpus_path=PATH_TO_MONACO)
NLP.add_pipe(annotation_reader_catma_, name="annotation_reader")


if __name__ == "__main__":
    # the annotated phenomena
    phenomena = [
        ("Comment", ["Einstellung", "Interpretation", "Meta"], "Reflexion IV"),
        ("NfR", ["Nichtfiktional", "Nichtfiktional+mK"], "Reflexion IV"),
        ("GI", ["BARE", "ALL", "MEIST", "EXIST", "DIV", "NEG"], "Reflexion IV"),
        ("Attribution", ["Figur", "Erzählinstanz", "Verdacht Autor", "Werkexterne Instanz"], "Attribution IV")
    ]

    for supertag, subtags, tagset in phenomena:

        # calculate agreements
        agreements = []
        for text in sorted(TEXTS_SENTS.keys()):
            path = os.path.join(PATH_TO_MONACO, text)
            if os.path.isdir(path):
                print(supertag, text)
                with open(os.path.join(path, text + ".txt")) as f:
                    
                    # pipe the text
                    doc = NLP(f.read())
                    sents = list(doc.sents)

                    # some texts have varying annotator constellations;
                    # these are the special cases:
                    if text == "Goethe__Die_Wahlverwandtschaften":
                        spans_list = [(0,152), (152,330), (330,486), (486,687)]
                    elif text == "Kleist__Michael_Kohlhaas":
                        spans_list = [(0,241), (241,443)]
                    elif text == "May__Winnetou_II":
                        spans_list = [(0,250), (250,500)]
                    elif text == "Novalis__Die_Lehrlinge_zu_Sais":
                        spans_list = [(0,230), (230,440)]
                    else:
                        spans_list = [(0,TEXTS_SENTS[text])]
                        # (otherwise it's just one span)
                    
                    # iterate over different agreement settings ...
                    for condition in ["multi", "binary"]:
                        
                        if condition == "binary":
                            if supertag == "Attribution":
                                continue
                            # in the "binary" condition; overwrite every tag with the supertag for the binary condition
                            for anno in doc._.annotations.keys():
                                annotations = doc._.annotations[anno].get_annotations(tagset=tagset, tags=tags)
                                for annotation in annotations:
                                    annotation.tag = supertag
                            tags = [supertag]
                        else:
                            tags = subtags

                        for units in ["tokens", "clauses"]:
                            for measure in ["kappa", "alpha", "gamma", "P", "R", "F"]:

                                # create an agreement entry
                                agreement = OrderedDict()
                                agreement["text"] = text
                                agreement["spans"] = ",".join([str(list(span)).replace(", ", ":") for span in spans_list])
                                agreement["annotators"] = []
                                agreement["condition"] = condition
                                agreement["units"] = units
                                agreement["measure"] = measure
                                agreement["value"] = [] # the values for each span go here

                                for start, end in spans_list:
                                    annotated_sents = sents[start:end]
                                    annotated_elements = {
                                        "tokens" : [token for sent in annotated_sents for token in sent],
                                        "clauses" : [clause for sent in annotated_sents for clause in sent._.clauses]
                                    }

                                    # determine the annotators
                                    annos = set()
                                    for element in annotated_elements[units]:
                                        for anno in element._.annotations:
                                            anno_annotations = element._.annotations[anno].get_annotations(tagset=tagset, tags=(["Attribution"] if supertag == "Attribution" else tags))
                                            if len(anno_annotations) > 0:
                                                annos.add(anno)
                                    annos.discard("GGG")

                                    # there must be either 2 or 6 annotators;
                                    # cases where there are only 0 or 1 annotators are treated separately below;
                                    # in this loop annotators are reduced to 2 by deleting least frequent annotators
                                    while len(annos) > 2 and len(annos) < 6 and supertag != "Attribution":
                                        min_count = None
                                        min_anno = None
                                        for anno in annos:
                                            count = sum([len(element._.annotations[anno].get_annotations(tagset=tagset, tags=(["Attribution"] if supertag == "Attribution" else tags))) for element in annotated_elements[units]])
                                            if (min_count is None) or (count < min_count):
                                                min_count = count
                                                min_anno = anno
                                        annos.remove(min_anno)

                                    annos = sorted(list(annos))
                                    agreement["annotators"].append(sorted(list(annos)))

                                    # agreement measures
                                    if measure in ["kappa", "alpha", "gamma"]:
                                        # if there are less than two annotators with annotations, set the agreement to NaN
                                        if len(annos) < 2:
                                            agreement["value"].append(np.nan)
                                        # if there are annotations for both (or all) annotators, compute the agreement
                                        else:
                                            if measure == "kappa":
                                                if supertag == "Attribution":
                                                    agreement["value"].append(return_fleiss_kappa(annotated_elements[units], [supertag], tagset, annos, "Attribution", None, "tokens_with_tag"))
                                                else:
                                                    agreement["value"].append(return_fleiss_kappa(annotated_elements[units], tags, tagset, annos))
                                            elif measure == "alpha":
                                                if supertag == "Attribution":
                                                    agreement["value"].append(return_krippendorff_alpha(annotated_elements[units], [supertag], tagset, annos, "Attribution", None, "tokens_with_tag"))
                                                else:
                                                    agreement["value"].append(return_krippendorff_alpha(annotated_elements[units], tags, tagset, annos))
                                            elif measure == "gamma":
                                                if supertag == "Attribution":
                                                    agreement["value"].append(return_mathet_gamma(annotated_elements[units], [supertag], tagset, annos, "Attribution", None, "tokens_with_tag", units=units, use_clauses_as_segments=False))
                                                else:
                                                    agreement["value"].append(return_mathet_gamma(annotated_elements[units], tags, tagset, annos, units=units, use_clauses_as_segments=False))

                                    # evaluation measures
                                    elif measure in ["P", "R", "F"] and supertag != "Attribution":
                                        # if there are no annotations for some annotators, add a `None` annotator
                                        # that has no annotations
                                        while len(annos) < 2:
                                            annos.append("AX")
                                        for anno in annos:
                                            counts = {tag : {"tp" : 0, "tn" : 0, "fp" : 0, "fn" : 0} for tag in tags+["micro"]}
                                            for element in annotated_elements[units]:
                                                gold = element._.annotations["GGG"].get_tags(tagset=tagset, tags=tags)
                                                if anno == "AX":
                                                    pred = set()
                                                else:
                                                    pred = element._.annotations[anno].get_tags(tagset=tagset, tags=tags)
                                                for tag in tags:
                                                    if tag in gold:
                                                        if tag in pred:
                                                            counts[tag]["tp"] += 1
                                                            counts["micro"]["tp"] += 1
                                                        else:
                                                            counts[tag]["fn"] += 1
                                                            counts["micro"]["fn"] += 1
                                                    else:
                                                        if tag in pred:
                                                            counts[tag]["fp"] += 1
                                                            counts["micro"]["fp"] += 1
                                                        else:
                                                            counts[tag]["tn"] += 1
                                                            counts["micro"]["tn"] += 1
                                            if measure == "P":
                                                agreement["value"].append(tagwise_precisions(counts)["macro"])
                                            elif measure == "R":
                                                agreement["value"].append(tagwise_recalls(counts)["macro"])
                                            elif measure == "F":
                                                agreement["value"].append(tagwise_fscores(counts)["macro"])
                                
                                agreement["std"] = np.nanstd(agreement["value"])
                                agreement["value"] = np.nanmean(agreement["value"]) # average the values
                                agreement["annotators"] = ",".join(["{" + ",".join(annos) + "}" for annos in agreement["annotators"]])
                                agreements.append(agreement)
        
        # add an average entry for every setting
        for condition in ["multi", "binary"]:
            for units in ["tokens", "clauses"]:
                for measure in ["kappa", "alpha", "gamma", "P", "R", "F"]:
                    a_list = [a["value"] for a in agreements if (a["condition"], a["units"], a["measure"]) == (condition, units, measure) and a["text"] != "AVERAGE"]
                    agreement = OrderedDict()
                    agreement["text"] = "AVERAGE"
                    agreement["spans"] = None
                    agreement["condition"] = condition
                    agreement["units"] = units
                    agreement["measure"] = measure
                    agreement["value"] = np.nanmean(a_list)
                    agreement["std"] = np.nanstd(a_list)
                    agreements.append(agreement)

        # write data to file
        df = pd.DataFrame.from_dict(agreements)
        df.to_csv(os.path.join(os.path.dirname(__file__), "agreements_" + supertag + ".csv"), index=False, header=True)