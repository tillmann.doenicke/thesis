import csv
import os


def update_tags(tags, sent, tag):
    """Update the tag counts.
    
    Args:
        tags (dict of str:(set of str)): For each tag, a set with the sentences that contain the tag.
        sent (str): The sentence ID.
        tag (str): The tag of the current clause.
    
    """
    if tag not in tags:
        tags[tag] = set()
    tags[tag].add(sent)


def update_sents(sents, sent, tag):
    """Update the coherent score of a sentence.
    
    Args:
        sents (dict of str:bool): Maps a sentence ID to a Boolean,
            `None` if the sentence does not contain any predictions.
            `True` if the sentence contains only coherent predictions.
            `False` if the sentence contains incoherent predictions.
        sent (str): The sentence ID.
        tag (str): The tag of the current clause.
    
    """
    if tag == "":
        c = None
    elif tag in ["AREL", "CAUS", "COMP", "CONJ", "CONT", "INFN", "MAIN", "PARE", "SENT", "THAT", "WHAT"]:
        c = True
    elif tag in ["FRAG", "LOUT", "RREL"]:
        c = False
    else:
        raise ValueError("Unknown tag:", tag)
    if (sent not in sents) or (sents[sent] is None) or (c is False):
        sents[sent] = c


def update_counts(counts, gold, pred, x=False):
    """Update the error matrix.
    
    Args:
        counts (dict of str:int): Maps the error name ("tp", "fp", "fn") to the error count.
        gold (bool): Whether the current clause is annotated as GI.
        pred (bool): Whether the current clause is predicted as GI.
        x (bool): Whether the current clause is allowed to be treated as either GI or non-GI.
    
    """
    if gold:
        if pred:
            counts["tp"] += 1
        elif x is True:
            pass # tn
        else:
            counts["fn"] += 1
    else:
        if not pred:
            pass # tn
        elif x is True:
            counts["tp"] += 1
        else:
            counts["fp"] += 1


def tcount(tags):
    """Calculate how often each tag occurs.
    
    Args:
        tags (dict of str:(set of str)): For each tag, a set with the sentences that contain the tag.
    
    Returns:
        list of (int,str): Tag frequency and tag, sorted by frequency in descending order.
    
    """
    tags = sorted([(len(tags[tag]), tag) for tag in tags], reverse=True)
    return tags


def cscore(sents):
    """Calculate the coherence score.
    
    Args:
        sents (dict of str:bool): Maps a sentence ID to a Boolean,
            `None` if the sentence does not contain any predictions.
            `True` if the sentence contains only coherent predictions.
            `False` if the sentence contains incoherent predictions.
    
    Returns:
        float: The number of sentences that only contain coherent predictions divided
            by the number of sentences that contain any predictions.
    
    """
    values = list(sents.values())
    s = 1.0*values.count(True)/(values.count(True)+values.count(False))
    return s


def fscore(counts):
    """Calculate the f1 score.
    
    Args:
        counts (dict of str:int): Maps the error name ("tp", "fp", "fn") to the error count.
    
    Returns:
        float: The f1 score.
    
    """
    p = 1.0*counts["tp"]/(counts["tp"]+counts["fp"])
    r = 1.0*counts["tp"]/(counts["tp"]+counts["fn"])
    f = 2.0*p*r/(p+r)
    return f


if __name__ == "__main__":
    for filename in os.listdir((os.path.dirname(__file__) if os.path.dirname(__file__) != "" else ".")):
        if filename.endswith("_edited.csv"):
            print(filename)
            tags0 = {}
            tags1 = {}
            tags2 = {}
            sents1 = {}
            sents2 = {}
            counts1 = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts2 = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts1a = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts2a = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts1b = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts2b = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts1c = {"tp" : 0, "fp" : 0, "fn" : 0}
            counts2c = {"tp" : 0, "fp" : 0, "fn" : 0}
            with open(os.path.join(os.path.dirname(__file__), filename), "r") as f:
                csvreader = csv.reader(f, delimiter=";")
                for row in list(csvreader)[1:]:
                    sent = row[0]
                    clause = row[2]
                    if clause != "":
                        gold = (row[3] != "")
                        pred1 = (row[4] != "")
                        pred2 = (row[5] != "")
                        tag0 = row[6]
                        tag1 = row[7]
                        tag2 = row[8]
                        x = (row[9] == "X")
                        ambig = (row[10] == "ambig")
                        if tag0 in ["THAT", "WHAT", "CAUS"]:
                            tag0 = "CONT"
                        if tag1 in ["THAT", "WHAT", "CAUS"]:
                            tag1 = "CONT"
                        if tag2 in ["THAT", "WHAT", "CAUS"]:
                            tag2 = "CONT"
                        update_tags(tags0, sent, tag0)
                        update_tags(tags1, sent, tag1)
                        update_tags(tags2, sent, tag2)
                        update_sents(sents1, sent, tag1)
                        update_sents(sents2, sent, tag2)
                        if not ambig:
                            update_counts(counts1, gold, pred1)
                            update_counts(counts2, gold, pred2)
                        update_counts(counts1a, gold, pred1, ambig)
                        update_counts(counts2a, gold, pred2, ambig)
                        update_counts(counts1b, gold, pred1, ambig or (x and tag0 == ""))
                        update_counts(counts2b, gold, pred2, ambig or (x and tag0 == ""))
                        update_counts(counts1c, gold, pred1, ambig or x)
                        update_counts(counts2c, gold, pred2, ambig or x)
            print("passages (gold):         \n", "\n".join(["\t" + str(t) for t in tcount(tags0)]))
            print("passages (neural):       \n", "\n".join(["\t" + str(t) for t in tcount(tags1)]))
            print("passages (flair):        \n", "\n".join(["\t" + str(t) for t in tcount(tags2)]))
            print("coherence score (neural):  ", format(cscore(sents1),".2f"))
            print("coherence score (flair):   ", format(cscore(sents2),".2f"))
            print("f1 score (neural):         ", format(fscore(counts1),".2f"))
            print("f1 score a (neural):       ", format(fscore(counts1a),".2f"))
            print("f1 score b (neural):       ", format(fscore(counts1b),".2f"))
            print("f1 score c (neural):       ", format(fscore(counts1c),".2f"))
            print("f1 score (flair):          ", format(fscore(counts2),".2f"))
            print("f1 score a (flair):        ", format(fscore(counts2a),".2f"))
            print("f1 score b (flair):        ", format(fscore(counts2b),".2f"))
            print("f1 score c (flair):        ", format(fscore(counts2c),".2f"))
            print()
            