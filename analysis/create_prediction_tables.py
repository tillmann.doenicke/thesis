import csv
import os
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from paths import PATH_TO_MONACO, PATH_TO_MONACO_2

sys.path.append(os.path.join(os.path.dirname(__file__), "..", "deep_learning"))
from tagger_evaluation import build_pipeline


if __name__ == "__main__":
    text_names = [
        (PATH_TO_MONACO, "Wieland__Geschichte_des_Agathon"),
        (PATH_TO_MONACO_2, "Bürger__Münchhausen")
    ]
    for path, text_name in text_names:
        with open(os.path.join(path, text_name, text_name + ".txt"), "r") as f_in:
            text = f_in.read()
            nlp1 = build_pipeline(700, "neural_gen_tagger", "binary", None, None, None, None)
            nlp2 = build_pipeline(700, "flair_gen_tagger", "binary", 100, "mixed1", None, None)
            doc1 = nlp1(text)
            doc2 = nlp2(text)
            annos = (lambda elem: elem._.annotations["GGG"].get_annotations(tags=["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"], tagset="Reflexion IV"))
            with open(os.path.join(os.path.dirname(__file__), text_name.split("__")[0] + ".csv"), "w") as f_out:
                csvwriter = csv.writer(f_out, delimiter=";")
                csvwriter.writerow(["sent", "clause", "text", "gold", "neural", "flair"])
                for j, (sent1, sent2) in enumerate(zip(doc1.sents, doc2.sents)):
                    csvwriter.writerow([str(j+1), "_", sent1.text, "", "", "", ""])
                    for k, (clause1, clause2) in enumerate(zip(sent1._.clauses, sent2._.clauses)):
                        text = " ".join([token.text for token in clause1._.tokens])
                        gs = ",".join([str(annos(doc1).index(anno)) for anno in annos(clause1)])
                        ps1 = ",".join([str(doc1._.gis.index(passage)) for passage in clause1._.gis])
                        ps2 = ",".join([str(doc2._.gis.index(passage)) for passage in clause2._.gis])
                        csvwriter.writerow([str(j+1), str(k+1), text, gs, ps1, ps2])