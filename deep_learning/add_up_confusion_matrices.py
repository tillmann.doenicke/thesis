import os
import sys

texts = ["Bürger", "Keller", "Knigge", "Kotzebue", "Lewald", "Lohenstein", "Nicolai", "Schiller", "Stockfleth", "Storm", "Wildermuth"]
taggers = ["rb_gen_tagger_multi", "clf_gen_tagger_multi", "neural_gen_tagger_multi", "flair_gen_tagger_multi_100_mixed1", "flair_gen_tagger_multi_100_mixed1_5"]

for tagger in taggers:
  confusion_matrix = None
  for text in texts:
    filename = text + "_" + tagger + ".txt"
    with open(os.path.join("results", filename)) as f:
      lines = f.readlines()
      lines = [line.strip() for line in lines]
      lines = lines[lines.index("")+1:]
      lines = [line.split() for line in lines]
      if confusion_matrix is None:
        lines = [[(int(x) if x.isnumeric() else x) for x in line] for line in lines]
        confusion_matrix = lines
      else:
        for i, line in enumerate(lines):
          for j, x in enumerate(line):
            if x.isnumeric():
              confusion_matrix[i][j] += int(x)
            elif confusion_matrix[i][j] != x:
              raise ValueError("Confusion matrix is incompatible:", filename)
  print(tagger)
  confusion_matrix[0].insert(0, "")
  for row in confusion_matrix:
    print("".join([" "*(10-len(x)) + x for x in [str(x) for x in row]]))
  print()