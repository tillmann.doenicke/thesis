              P         R         F
ALL    0.264706  0.195652  0.225000
BARE   0.730769  0.174312  0.281481
DIV    0.222222  0.103448  0.141176
EXIST  0.000000  0.000000  0.000000
MEIST  0.000000  0.000000  0.000000
NEG    0.409091  0.183673  0.253521
micro  0.344000  0.164122  0.222222
macro  0.271131  0.109514  0.150197
binar  0.680000  0.333333  0.447368

          ALL  BARE  DIV  EXIST  MEIST  NEG  untagged  multiple
ALL         7     1    3      0      3    0        30         0
BARE        3    16    8      0      2    1        73         0
DIV         3     0    6      1      4    4        35         0
EXIST       0     0    0      0      0    0         0         0
MEIST       0     0    0      0      0    0         0         0
NEG         7     1    1      0      0    8        31         0
untagged   12     5    9      0      6    8      1412         0
multiple    2     3    0      0      0    1         1         0
