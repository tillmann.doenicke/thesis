# evaluate on individual test sets
python tagger_evaluation.py Fontane flair_gen_tagger both 100 mixed1 0
python tagger_evaluation.py Goethe flair_gen_tagger both 100 mixed1 1
python tagger_evaluation.py Grillparzer flair_gen_tagger both 100 mixed1 2
python tagger_evaluation.py Hölderlin flair_gen_tagger both 100 mixed1 3
python tagger_evaluation.py Kleist flair_gen_tagger both 100 mixed1 4
python tagger_evaluation.py May flair_gen_tagger both 100 mixed1 5
python tagger_evaluation.py Musil flair_gen_tagger both 100 mixed1 6
python tagger_evaluation.py Novalis flair_gen_tagger both 100 mixed1 7
python tagger_evaluation.py Reuter flair_gen_tagger both 100 mixed1 8
python tagger_evaluation.py Rilke flair_gen_tagger both 100 mixed1 9

# evaluate on common test set
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 0
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 1
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 2
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 3
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 4
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 5
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 6
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 7
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 8
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1 9
