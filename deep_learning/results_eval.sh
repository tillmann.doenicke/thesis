# evaluate human annotators
python tagger_evaluation.py Wieland A1 multi
python tagger_evaluation.py Wieland A2 multi
python tagger_evaluation.py Wieland A3 multi
python tagger_evaluation.py Wieland A4 multi
python tagger_evaluation.py Wieland A5 multi
python tagger_evaluation.py Wieland A6 multi

# evaluate taggers
python tagger_evaluation.py Wieland rb_gen_tagger multi
python tagger_evaluation.py Wieland clf_gen_tagger multi
python tagger_evaluation.py Wieland neural_gen_tagger both
python tagger_evaluation.py Wieland flair_gen_tagger both 100 mixed1
