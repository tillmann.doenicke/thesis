# evaluate human annotators
python tagger_evaluation.py Seghers A1 multi
python tagger_evaluation.py Seghers A2 multi
python tagger_evaluation.py Seghers A3 multi
python tagger_evaluation.py Seghers A4 multi
python tagger_evaluation.py Seghers A5 multi
python tagger_evaluation.py Seghers A6 multi

# evaluate taggers
python tagger_evaluation.py Seghers rb_gen_tagger multi
python tagger_evaluation.py Seghers clf_gen_tagger multi
python tagger_evaluation.py Seghers neural_gen_tagger both
python tagger_evaluation.py Seghers flair_gen_tagger both 100 mixed1
