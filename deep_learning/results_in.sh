# define texts and annotators
texts=( Bürger:A4,A5 Keller:A6,A12 Knigge:A2,A4 Kotzebue:A1,A4 Lewald:A1,A4 Lohenstein:A2,A3 Nicolai:A2,A11 Schiller:A2,A5 Stockfleth:A7,A9 Storm:A3,A6 Wernicke:A5,A6 Wildermuth:A1,A3 )
for text in "${texts[@]}"
do
    IFS=':'
    read -a textarr <<< "$text"
    text=${textarr[0]}
    annostr=${textarr[1]}
    IFS=','
    read -a annos <<< "$annostr"
    IFS=' '
    for anno in "${annos[@]}"
    do
        # evaluate human annotators
        python tagger_evaluation.py "$text" "$anno" multi
    done
    # evaluate taggers
    python tagger_evaluation.py "$text" rb_gen_tagger multi
    python tagger_evaluation.py "$text" clf_gen_tagger multi
    python tagger_evaluation.py "$text" neural_gen_tagger both
    python tagger_evaluation.py "$text" flair_gen_tagger both 100 mixed1
    python tagger_evaluation.py "$text" flair_gen_tagger both 100 mixed1 5
done
