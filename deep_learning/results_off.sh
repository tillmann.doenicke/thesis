# evaluate human annotators
python tagger_evaluation.py SITENT A1 binary
python tagger_evaluation.py SITENT A2 binary

# evaluate taggers
python tagger_evaluation.py SITENT rb_gen_tagger multi
python tagger_evaluation.py SITENT clf_gen_tagger multi
python tagger_evaluation.py SITENT neural_gen_tagger multi
python tagger_evaluation.py SITENT neural_gen_tagger binary
python tagger_evaluation.py SITENT flair_gen_tagger multi 100 mixed1
python tagger_evaluation.py SITENT flair_gen_tagger binary 100 mixed1
python tagger_evaluation.py SITENT flair_gen_tagger multi 100 mixed1 5
python tagger_evaluation.py SITENT flair_gen_tagger binary 100 mixed1 5
