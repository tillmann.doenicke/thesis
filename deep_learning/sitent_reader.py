import re
from csv import DictReader, QUOTE_NONE


def read_sitent_file(path):
    """Reads a CSV file from the SITENT corpus.

    Args:
        path (str): Path to the CSV file.
    
    Returns:
        str: The text.
        list of (int,int): The character positions for each clause.
        list of (set of str): The set of labels for each clause.
    
    """
    text = ""
    clause_ids = []
    clause_labels = []
    with open(path, "r") as f:
        dict_reader = list(DictReader(f, delimiter="\t", doublequote=False))
        # some files are missing a name for the first column, so that the column names are incorrect,
        # we use an offset to fix that (use `row[col_names[col_names.index(col_name)+offset]]` instead of `row[col_name]`):
        col_names = list(dict_reader[0].keys())
        offset = (0 if col_names[0].endswith("ID") or col_names[0].endswith("#") else 1)
        for i, row in enumerate(dict_reader):
            clause = row[col_names[col_names.index("text")+offset]]
            clause = clause.replace('"', "")    # remove doublequotes
            clause = re.sub("\s+", " ", clause) # remove unnecessary whitespace
            clause = clause.strip()             # remove surrounding whitespace
            if clause != "":
                ids = (len(text), len(text)+len(clause))
                labels = {}
                # gold annotation:
                labels["GGG"] = row[col_names[col_names.index("gold_SitEntType")+offset]]
                # other annotations:
                for a in range(2):
                    anno = "A" + str(a+1)
                    anno_cols = ["ANNO" + str(a+1), "SITUATION_ENTITY_" + chr(65+a), chr(65+a) + "_Sitent_Type"]
                    if len(set(row.keys()).intersection(set(anno_cols))) != 1 and len(labels) == 1:
                        raise ValueError("Annotation column not found in file", path)
                    for anno_col in anno_cols:
                        if anno_col in row:
                            labels[anno] = row[col_names[col_names.index(anno_col)+offset]]
                            break
                labels = {anno : (set() if (labels[anno] is None) or (labels[anno] == "") else set(labels[anno].split(", "))) for anno in labels}
                # append clause and a space:
                sep = " "
                text += (clause + sep)
                clause_ids.append(ids)
                clause_labels.append(labels)
            else:
                # separate documents within a text by newlines
                text += "\n\n"
    text = text.rstrip()
    return text, clause_ids, clause_labels


if __name__ == "__main__":
    import os
    import sys
    sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
    from paths import PATH_TO_SITENT
    text, clause_ids, clause_labels = read_sitent_file(os.path.join(PATH_TO_SITENT, "test_german_genre", "argumentative_microtexts+genre+modalsverbs.csv"))
    for i, clause_id in enumerate(clause_ids):
        start, end = clause_id
        print(start, end)
        print(text[start:end])
        print(clause_labels[i])
        input()