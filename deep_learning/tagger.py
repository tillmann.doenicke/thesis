import os
import sys
from flair.data import Corpus, Sentence, Token
from flair.datasets.sequence_labeling import MultiFileColumnCorpus
from flair.embeddings import BertEmbeddings, TransformerWordEmbeddings
from flair.models import SequenceTagger
from flair.trainers import ModelTrainer
from torch.optim.sgd import SGD

from imbalanced_sampler import ImbalancedClassificationDatasetSampler
from lamb import Lamb

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from paths import PATH_TO_MONACO
from texts import TEXTS_SENTS


def load_model(label_condition, context_tokens, hyperparameter_setup, cv=None):
    """Load a GI tagger with the given parameters. Train it if it does not exist.

    Args:
        label_condition (str): "multi" or "binary"
        context_tokens (int): Use chunks of `context_tokens` tokens.
            If `None`, use sentences as chunks.
        hyperparameter_setup (str): Defines the hyperparameters to use.
            "speech": Use the parameters from Brunner et al. (2020)
            "gi" : Use the parameters from Schomacker et al. (2022)
            "mixed[1234]": Use a mixed setup.
        cv (int): Iteration of cross-validation.
    
    Returns:
        `SequenceTagger`: flair model.
    
    """
    if hyperparameter_setup == "gi":
        embeddings = BertEmbeddings("deepset/gbert-large")      # gi
        dropout = 0.3                                           # gi
        optimizer = Lamb                                        # gi
        learning_rate = 0.0001                                  # gi
        max_epochs = 20                                         # gi
        patience = max_epochs                                   # gi
    elif hyperparameter_setup == "mixed1":
        embeddings = BertEmbeddings("deepset/gbert-large")      # gi
        dropout = 0.0                                           # speech
        optimizer = SGD                                         # speech
        learning_rate = 0.1                                     # speech
        max_epochs = 150                                        # speech
        patience = 3                                            # speech
    elif hyperparameter_setup == "mixed2":
        embeddings = BertEmbeddings("deepset/gbert-large")      # gi
        dropout = 0.0                                           # speech
        optimizer = Lamb                                        # gi
        learning_rate = 0.1                                     # speech
        max_epochs = 150                                        # speech
        patience = 3                                            # speech
    elif hyperparameter_setup == "mixed3":
        embeddings = BertEmbeddings("deepset/gbert-large")      # gi
        dropout = 0.3                                           # gi
        optimizer = SGD                                         # speech
        learning_rate = 0.1                                     # speech
        max_epochs = 150                                        # speech
        patience = 3                                            # speech
    elif hyperparameter_setup == "mixed4":
        embeddings = BertEmbeddings("deepset/gbert-large")      # gi
        dropout = 0.3                                           # gi
        optimizer = Lamb                                        # gi
        learning_rate = 0.1                                     # speech
        max_epochs = 150                                        # speech
        patience = 3                                            # speech
    else:
        embeddings = BertEmbeddings("bert-base-german-cased")   # speech
        dropout = 0.0                                           # speech
        optimizer = SGD                                         # speech
        learning_rate = 0.1                                     # speech
        max_epochs = 150                                        # speech
        patience = 3                                            # speech
    
    model_name = "_".join([str(x) for x in [label_condition, context_tokens, hyperparameter_setup]])
    if cv is None:
        # normal training
        model_path = os.path.join(os.path.dirname(__file__), "models", model_name)
    else:
        # cross-validation
        cv = min(cv, 9)
        model_path = os.path.join(os.path.dirname(__file__), "models", "cv", model_name + "_" + str(cv))

    if not os.path.exists(model_path):

        # define train/dev/text split
        if cv is None:
            # normal training
            test_texts = ["Wieland__Geschichte_des_Agathon"]
            dev_texts = ["Fontane__Der_Stechlin", "Gellert__Das_Leben_der_schwedischen_Gräfin_von_G"]
        else:
            # cross-validation
            cv_test_texts = [
                ["Fontane__Der_Stechlin"], 
                ["Goethe__Die_Wahlverwandtschaften"], 
                ["Grillparzer__Der_arme_Spielmann"], 
                ["Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland"],
                ["Kleist__Michael_Kohlhaas"],
                ["May__Winnetou_II"],
                ["Musil__Der_Mann_ohne_Eigenschaften"],
                ["Novalis__Die_Lehrlinge_zu_Sais"],
                ["Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande"],
                ["Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge"]
            ]
            cv_dev_texts = [
                ["Dahn__Kampf_um_Rom_ab_Kapitel_2", "LaRoche__Geschichte_des_Fräuleins_von_Sternheim"],
                ["Fontane__Der_Stechlin", "May__Winnetou_II"],
                ["Gellert__Das_Leben_der_schwedischen_Gräfin_von_G", "Musil__Der_Mann_ohne_Eigenschaften"],
                ["Goethe__Die_Wahlverwandtschaften", "Novalis__Die_Lehrlinge_zu_Sais"],
                ["Grillparzer__Der_arme_Spielmann", "Reuter__Schelmuffskys_kuriose_und_sehr_gefährliche_Reisebeschreibung_zu_Wasser_und_Lande"],
                ["Grimmelshausen__Der_abenteuerliche_Simplicissimus", "Rilke__Die_Aufzeichnungen_des_Malte_Laurids_Brigge"],
                ["Hoffmann__Der_Sandmann", "Schnabel__Die_Insel_Felsenburg"],
                ["Hölderlin__Hyperion_oder_der_Eremit_in_Griechenland", "Dahn__Kampf_um_Rom_ab_Kapitel_2"],
                ["Kafka__Der_Bau", "Fontane__Der_Stechlin"],
                ["Kleist__Michael_Kohlhaas", "Gellert__Das_Leben_der_schwedischen_Gräfin_von_G"]
            ]
            test_texts = cv_test_texts[cv]
            dev_texts = cv_dev_texts[cv]
        train_texts = [text for text in sorted(TEXTS_SENTS.keys()) if text not in (test_texts+dev_texts)]

        # create mappings from the GI annotations in the MONACO conllu files to labels;
        # e.g. "42:ALL,43:DIV[!]" -> "DIV";
        # construct all possible combinations of one or two tags and map them to the last tag
        label_name_map = {}
        tags = ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"]
        n = 500 # maximum passage index
        d = 10 # maximum difference of indices between two passages at the same token
        for i1 in range(n):
            for tag1 in tags:
                for ambig1 in ["", "[!]"]:
                    for i2 in range(max(0, i1-d), min(i1+d, n)):
                        for tag2 in tags:
                            for ambig2 in ["", "[!]"]:
                                label = str(i1) + ":" + tag1 + ambig1
                                name = tag1
                                if not (i2 == i1 and tag2 == tag1 and ambig2 == ambig1):
                                    label += "," + str(i2) + ":" + tag2 + ambig2
                                    name = tag2
                                label_name_map[label] = name

        # there is only one occurrence of a three-label annotation in the corpus;
        # add this manually here
        label_name_map["17:BARE,18:BARE,19:ALL"] = "ALL"

        # in the binary condition, convert all GI tags to "GI"
        if label_condition == "binary":
            for label in label_name_map:
                label_name_map[label] = "GI"

        # "_" means no label (not GI)
        label_name_map["_"] = "-"

        # read the MONACO files
        corpus = MultiFileColumnCorpus(
            column_format={1 : "text", 10 : "gi_label"},
            train_files=[os.path.join(PATH_TO_MONACO, text, text + "_tabs.conllu") for text in train_texts],
            test_files=[os.path.join(PATH_TO_MONACO, text, text + "_tabs.conllu") for text in test_texts],
            dev_files=[os.path.join(PATH_TO_MONACO, text, text + "_tabs.conllu") for text in dev_texts],
            column_delimiter=r"\t",
            comment_symbol="#",
            document_separator_token=None,
            skip_first_line=False,
            in_memory=True,
            label_name_map=label_name_map
        )
        print(len(corpus.train), len(corpus.dev), len(corpus.test))

        # in the `context_tokens` condition, create a new corpus with longer "sentences";
        # a sentence is now a text chunk of up to 100 tokens
        if context_tokens is not None:
            n = context_tokens # maximum number of tokens per text chunk
            sentence_lists = {}
            for attr in ["train", "test", "dev"]:
                sentences = getattr(corpus, attr)
                new_sentences = []
                new_sentence = Sentence("")
                for sentence in sentences:
                    if (len(new_sentence) + len(sentence) > n or sentence.previous_sentence() is None) and len(new_sentence) > 0:
                        new_sentences.append(new_sentence)
                        new_sentence = Sentence("")
                    tokens = [token for token in sentence]
                    for token in tokens:
                        if len(new_sentence) == n:
                            new_sentences.append(new_sentence)
                            new_sentence = Sentence("")
                        new_token = Token(token.text)
                        new_token.add_label("gi_label", token.get_label("gi_label").value)
                        new_sentence.add_token(new_token)
                if len(new_sentence) > 0:
                    new_sentences.append(new_sentence)
                sentence_lists[attr] = new_sentences
            corpus = Corpus(sentence_lists["train"], sentence_lists["dev"], sentence_lists["test"])
        print(len(corpus.train), len(corpus.dev), len(corpus.test))

        # create a label dictionary from the corpus; remove the "<unk>" tag
        label_dictionary = corpus.make_label_dictionary(label_type="gi_label")
        #label_dictionary.remove_item("<unk>")
        print(label_dictionary)

        # create a sequence tagger
        tagger = SequenceTagger(
            dropout=dropout, # default: 0.0
            embeddings=embeddings,
            hidden_size=256, # default: 256
            rnn_layers=2, # default: 1
            tag_dictionary=label_dictionary,
            tag_type="gi_label",
            use_crf=True # default: True
        )

        # train the tagger on the corpus
        trainer = ModelTrainer(tagger, corpus)
        trainer.train(
            model_path,
            checkpoint=False, # default: False
            exclude_labels=[], # default: []
            learning_rate=learning_rate, # default: 0.1
            main_evaluation_metric=(("macro avg" if label_condition == "multi" else "micro avg"), "f1-score"), # default: ("micro avg", "f1 score")
            max_epochs=max_epochs, # default: 100
            min_learning_rate=0.0001, # default: 0.0001
            mini_batch_size=8, # default: 32
            optimizer=optimizer, # default: SGD
            patience=patience, # default: 3
            sampler=(ImbalancedClassificationDatasetSampler if label_condition == "multi" else None) # default: None
        )

    # load the tagger
    model = SequenceTagger.load(os.path.join(model_path, "final-model.pt"))
    return model


if __name__ == "__main__":
    model = load_model("binary", 100, "speech")

    sentence = Sentence("Hans versuchte , einen Hasen zu fangen . Hasen sind schnell , aber schlussendlich hat er ihn erwischt .", use_tokenizer=False)
    model.predict(sentence)
    print(sentence.to_tagged_string())
    print()