import os
import pandas as pd
import spacy
import sys
from flair.data import Sentence
from spacy.tokens import Doc, Span, Token

from sitent_reader import read_sitent_file
from tagger import load_model

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from texts import TEXTS_SENTS, TEXTS_SENTS_2
from paths import PATH_TO_MONACO, PATH_TO_MONACO_2, PATH_TO_MONAPIPE, PATH_TO_SITENT

sys.path.append(PATH_TO_MONAPIPE)
from evaluation.measures import *
from pipeline.annotation import Annotation, AnnotationList
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from pipeline.components.clausizer import dependency_clausizer, strip_punct_and_convert
from pipeline.components.coref import rb_coref
from pipeline.components.gen_tagger import clf_gen_tagger, neural_gen_tagger, rb_gen_tagger
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.slicer import max_sent_slicer
from pipeline.components.speaker_extractor import rb_speaker_extractor
from pipeline.components.speech_tagger import quotation_marks_speech_tagger
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.global_constants import SPACY_MAX_LENGTH
from pipeline.utils_methods import add_extension, get_doc_text, transfer_labels_to_passages
from settings import PARSING_PATH, PICKLE_PATH


def sitent_data_reader(doc, sitent_data, format_tags=True):
    """Spacy pipeline component.
        Reads the data from a SITENT file:
            - adds clause spans
            - adds sentence starts
            - adds annotations

    Args:
        doc (`Doc`): A spacy document object.
        sitent_data (dict of str:(list of obj)): The data from the SITENT corpus.
        format_tags (boolean): If True, converts the SITENT tags to MONACO tags. If False, keeps the original SITENT tags.
    
    Returns:
        doc (`Doc`): A spacy document object.
    
    """
    add_extension(Doc, "clauses")
    add_extension(Span, "clauses")
    add_extension(Span, "tokens")
    add_extension(Span, "prec_punct")
    add_extension(Span, "succ_punct")
    add_extension(Token, "clause")
    add_extension(Token, "annotations")
    add_extension(Span, "annotations")
    add_extension(Doc, "annotations")

    # get the data for the currently piped text
    text = get_doc_text(doc, normalized=False)
    text_data = None
    for text_name in sitent_data.keys():
        if sitent_data[text_name]["text"] == text:
            text_data = sitent_data[text_name]
            break
    if text_data is None:
        raise ValueError("No SITENT data found for text `", text[:10], "...`")
    
    # create a dictionary that maps character positions to token indices;
    # instantiate `token._.annotations` with an empty dictionary;
    # set `token.is_sent_start` to `False` for every token
    charpos_to_token_i = {}
    for token_i, token in enumerate(doc):
        try:
            charpos = token._.idx
        except AttributeError:
            charpos = token.idx
        charpos_to_token_i[charpos] = token_i
        token._.annotations = {}
        token.is_sent_start = False
    
    # get the annotation names ("GGG", "A1", ...)
    annos = []
    for clause_label in text_data["clause_labels"]:
        annos = sorted(clause_label.keys())
        break

    # create empty `AnnotationList`s for the document and all tokens
    doc._.annotations = {}
    for anno in annos:
        doc._.annotations[anno] = AnnotationList()
        for token in doc:
            token._.annotations[anno] = AnnotationList()

    clauses = []
    next_sent_start = True
    for clause_id in text_data["clause_ids"]:
        clause_start, clause_end = clause_id
        clause_range = range(clause_start, clause_end)

        # get the tokens of the clause
        clause_tokens = []
        for charpos in clause_range:
            try:
                clause_tokens.append(doc[charpos_to_token_i[charpos]])
            except KeyError:
                pass
        
        if len(clause_tokens) > 0:
            # if the clause is the start of a new sentence,
            # set `token.is_sent_start` to `True` for the first token
            if next_sent_start:
                clause_tokens[0].is_sent_start = True
            
            # determine whether the clause is the last clause of a sentence
            next_sent_start = False
            for token in reversed(clause_tokens):
                if not (token.is_punct or token.is_space):
                    break
                if token.text in [".", "!", "?", ";", ":"]:
                    next_sent_start = True
                    break
        
        clauses.append(clause_tokens)
    
    # convert the clauses (lists of tokens) to `Span` objects
    clauses = strip_punct_and_convert(doc, clauses)

    # add the "GI" annotations to all clauses and tokens
    for clause, clause_label in zip(clauses, text_data["clause_labels"]):
        clause._.annotations = {}
        for anno in annos:
            clause._.annotations[anno] = AnnotationList()
            for se_type in clause_label[anno]:
                if format_tags and se_type not in ["GENERALIZING_SENTENCE", "GENERIC_SENTENCE"]:
                    continue
                annotation = Annotation(
                    tag=("GI" if format_tags else se_type), 
                    tagset=("Reflexion IV" if format_tags else "SITENT"), 
                    property_values_dict={},
                    tokens=clause_tokens,
                    clauses=[clause],
                    id_value=None, 
                    strings=None,
                    string_positions=None
                )
                clause._.annotations[anno].append(annotation)
                for token in clause._.tokens:
                    token._.annotations[anno].append(annotation)

    # remove empty clauses
    clauses = [clause for clause in clauses if len(clause) > 0]

    doc._.clauses = clauses
    for sent in doc.sents:
        sent._.clauses = []
    for clause in clauses:
        # add the clause to its sentence's clauses:
        clause._.tokens[0].sent._.clauses.append(clause)
        # add the clause to its tokens:
        for token in clause._.tokens:
            token._.clause = clause
        # add the annotations to the document:
        for anno in annos:
            for annotation in clause._.annotations[anno]:
                doc._.annotations[anno].append(annotation)

    return doc


def flair_gen_tagger(doc, label_condition, context_tokens, hyperparameter_setup, cv=None):
    """Spacy pipeline component.
        A gen_tagger that loads and uses a flair model.

    Args:
        doc (`Doc`): A spacy document object.
        label_condition (str): "multi" or "binary"
        context_tokens (int): Use chunks of `context_tokens` tokens.
        hyperparameter_setup (str): Defines the hyperparameters to use.
        cv (int): Iteration of cross-validation.
    
    Returns:
        doc (`Doc`): A spacy document object.
    
    """
    add_extension(Span, "gis")
    add_extension(Doc, "gis")

    model = load_model(label_condition, context_tokens, hyperparameter_setup, cv)

    all_clause_labels = {}

    if context_tokens is None:
        for sent in doc.sents:
            text = " ".join([token.text for token in sent if not token.is_space])
            text = Sentence(text, use_tokenizer=False)
            clause_labels = get_clause_labels(sent, text, model)
            for clause in clause_labels:
                all_clause_labels[clause] = clause_labels[clause]
    else:
        chunks = []
        chunk = []
        for sent in doc.sents:
            tokens = list(sent)
            if len(chunk) + len(tokens) <= context_tokens or len(chunk) == 0:
                chunk.extend(tokens)
            else:
                chunks.append(chunk)
                chunk = tokens
        if len(chunk) > 0:
            chunks.append(chunk)
        for chunk in chunks:
            text = " ".join([token.text for token in chunk if not token.is_space])
            text = Sentence(text, use_tokenizer=False)
            clause_labels = get_clause_labels(chunk, text, model)
            for clause in clause_labels:
                all_clause_labels[clause] = clause_labels[clause]
    
    labels = []
    for clause in doc._.clauses:
        try:
            labels.append(all_clause_labels[clause])
        except KeyError:
            labels.append(set())
    
    transfer_labels_to_passages(doc, "gis", labels)
    
    return doc


def get_clause_labels(sent, text, tagger):
    """Get the labels for each clause in a sentence.

    Args:
        sent (`Span`): The sentence in spacy format.
        text (`Sentence`): The sentence in flair format.
        tagger (`SequenceTagger`): The flair tagger model.
    
    Returns:
        dict of `Span`:(set of str): A dictionary that maps clauses to labels.
    
    """
    try:
        tagger.predict(text)
        clause_tags = {token._.clause : {} for token in sent if token._.clause is not None}
        s = 0
        for i, token in enumerate(sent):
            if token.is_space:
                s += 1
            else:
                tag = text.tokens[i-s].get_labels()[0].value
                if tag != "-":
                    if token._.clause is not None:
                        if tag not in clause_tags[token._.clause]:
                            clause_tags[token._.clause][tag] = 0
                        clause_tags[token._.clause][tag] += 1
        for clause in clause_tags:
            clause_tags[clause] = set([tag for tag in clause_tags[clause] if 1.0*clause_tags[clause][tag]/len(clause._.tokens) >= 0.5])
        return clause_tags
    except RuntimeError: # input sequence too long
        return {token._.clause : set() for token in sent if token._.clause is not None}


def annotator_gen_tagger(doc, annotator, single_label=False):
    """Spacy pipeline component.
        A (fake) gen_tagger that predicts the annotations of a given annotator.

    Args:
        doc (`Doc`): A spacy document object.
        annotator (str): The annotator (e.g. "A1").
    
    Returns:
        doc (`Doc`): A spacy document object.
    
    """
    add_extension(Span, "gis")
    add_extension(Doc, "gis")

    labels = []

    for clause in doc._.clauses:
        tags = []
        for annotation in clause._.annotations[annotator].get_annotations(tagset="Reflexion IV", tags=["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG", "GI"]):
            tags.append(annotation.tag)
        if len(tags) > 0 and single_label:
            tags = [tags[-1]]
        tags = set(tags)
        labels.append(tags)
    
    transfer_labels_to_passages(doc, "gis", labels)

    return doc


def build_pipeline(max_sents, gen_tagger, label_condition, context_tokens=None, hyperparameter_setup=None, cv=None, sitent_data=None):
    """Builds a spaCy pipeline with the required gen_tagger.

    Args:
        max_sents (int): Number of sentences after which to cut off the document.
        gen_tagger (str): Name of the gen_tagger to include.
        label_condition (str): "multi" or "binary"
        context_tokens (int): Only for `flair_gen_tagger`, use chunks of `context_tokens` tokens.
        hyperparameter_setup (str): Only for `flair_gen_tagger`, defines the hyperparameters to use.
        cv (int): Only for `flair_gen_tagger`, iteration of cross-validation.
        sitent_data (dict of str:(list of obj)): If not `None`, this indicates that the data to parse comes from the SITENT corpus.
            In this case, a special clausizer and annotation reader is used which need the information from the `sitent_data` dict.
    
    Returns:
        `spacy`: A spaCy pipeline.
    
    """
    model = os.path.join(PARSING_PATH, "de_ud_lg")
    nlp = spacy.load(model)
    nlp.max_length = SPACY_MAX_LENGTH

    def pw(doc, func, **kwargs):
        return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)

    pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=max_sents)
    nlp.add_pipe(pickle_init_, name="pickle_init", before="tagger")

    dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
    nlp.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

    lemma_fixer_ = lambda doc: pw(doc, lemma_fixer)
    nlp.add_pipe(lemma_fixer_, name="lemma_fixer", after="tagger")

    if sitent_data is None:
        
        spacy_sentencizer_ = lambda doc: pw(doc, spacy_sentencizer)
        nlp.add_pipe(spacy_sentencizer_, name="sentencizer", before="parser")

        max_sent_slicer_ = lambda doc: pw(doc, max_sent_slicer)
        nlp.add_pipe(max_sent_slicer_, name="slicer", after="parser")

        dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
        nlp.add_pipe(dependency_clausizer_, name="clausizer")

    else:

        sitent_data_reader_ = lambda doc: pw(doc, sitent_data_reader, sitent_data=sitent_data)
        nlp.add_pipe(sitent_data_reader_, name="sitent_data_reader", before="parser")

    if gen_tagger in ["rb_gen_tagger", "clf_gen_tagger"]:

        demorphy_analyzer_ = lambda doc: pw(doc, demorphy_analyzer)
        nlp.add_pipe(demorphy_analyzer_, name="analyzer")

        rb_tense_tagger_ = lambda doc: pw(doc, rb_tense_tagger)
        nlp.add_pipe(rb_tense_tagger_, name="tense_tagger")

        quotation_marks_speech_tagger_ = lambda doc: pw(doc, quotation_marks_speech_tagger)
        nlp.add_pipe(quotation_marks_speech_tagger_, name="speech_tagger")

        rb_speaker_extractor_ = lambda doc: pw(doc, rb_speaker_extractor)
        nlp.add_pipe(rb_speaker_extractor_, name="speaker_extractor")

        rb_coref_ = lambda doc: pw(doc, rb_coref)
        nlp.add_pipe(rb_coref_, name="coref")

    if gen_tagger == "rb_gen_tagger":

        rb_gen_tagger_ = lambda doc: pw(doc, rb_gen_tagger)
        nlp.add_pipe(rb_gen_tagger_, name="gen_tagger")
    
    elif gen_tagger == "clf_gen_tagger":

        clf_gen_tagger_ = lambda doc: pw(doc, clf_gen_tagger)
        nlp.add_pipe(clf_gen_tagger_, name="gen_tagger")
    
    elif gen_tagger == "neural_gen_tagger":

        neural_gen_tagger_ = lambda doc: neural_gen_tagger(doc, label_condition=label_condition)
        nlp.add_pipe(neural_gen_tagger_, name="gen_tagger")
    
    elif gen_tagger == "flair_gen_tagger":

        flair_gen_tagger_ = lambda doc: flair_gen_tagger(doc, label_condition=label_condition, context_tokens=context_tokens, hyperparameter_setup=hyperparameter_setup, cv=cv)
        nlp.add_pipe(flair_gen_tagger_, name="gen_tagger")
    
    if sitent_data is None:
    
        annotation_reader_catma_1 = lambda doc: annotation_reader_catma(doc, corpus_path=PATH_TO_MONACO)
        nlp.add_pipe(annotation_reader_catma_1, name="annotation_reader_1")

        annotation_reader_catma_2 = lambda doc: annotation_reader_catma(doc, corpus_path=PATH_TO_MONACO_2)
        nlp.add_pipe(annotation_reader_catma_2, name="annotation_reader_2")

    if gen_tagger in ["A1", "A2", "A3", "A4", "A5", "A6", "A7", "A8", "A9", "A10", "A11", "A12", "GGG"]:

        annotator_gen_tagger_ = lambda doc: annotator_gen_tagger(doc, gen_tagger)
        nlp.add_pipe(annotator_gen_tagger_, name="gen_tagger")

    return nlp


def get_scores(docs, end_of_annotations, label_condition):
    """TODO
    """
    counts, confusions = compare_gold_pred_clauses(
        docs=docs,
        attribute="gis",
        annotator="GGG",
        tagset="Reflexion IV",
        tags=(["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"] + (["GI"] if label_condition == "binary" else [])),
        supertag=("GI" if label_condition == "binary" else None),
        end_of_annotations=end_of_annotations,
        exclude_ambiguous=True
    )
    precisions = tagwise_precisions(counts)
    recalls = tagwise_recalls(counts)
    fscores = tagwise_fscores(counts)
    return precisions, recalls, fscores, confusions


if __name__ == "__main__":
    args = sys.argv
    if len(args) > 3:
        test_texts = args[1].split(",")
        gen_tagger = args[2]
        label_condition = args[3]
        context_tokens = None
        hyperparameter_setup = None
        cv = None
        if len(args) > 5:
            context_tokens = (None if args[4] == "None" else int(args[4]))
            hyperparameter_setup = args[5]
            if len(args) > 6:
                cv = int(args[6])
        
        if label_condition == "both":
            label_conditions = ["multi", "binary"]
        else:
            label_conditions = [label_condition]

        texts = []
        text_names = []
        max_sents = 700
        sitent_data = None
        if args[1] == "SITENT":
            max_sents = -1
            sitent_data = {}
            sitent_test_path = os.path.join(PATH_TO_SITENT, "test_german_genre")
            test_texts = sorted([test_text for test_text in os.listdir(sitent_test_path) if test_text.endswith(".csv")])
            for test_text in test_texts:
                text_name = test_text[:-4]
                path = os.path.join(sitent_test_path, test_text)
                text, clause_ids, clause_labels = read_sitent_file(path)
                texts.append(text)
                text_names.append(text_name)
                sitent_data[text_name] = {"text" : text, "clause_ids" : clause_ids, "clause_labels" : clause_labels}
        else:
            for test_text in test_texts:
                for texts_sents, path_to_monaco in zip([TEXTS_SENTS, TEXTS_SENTS_2], [PATH_TO_MONACO, PATH_TO_MONACO_2]):
                    for text_name in sorted(texts_sents.keys()):
                        if text_name.startswith(test_text):
                            path = os.path.join(path_to_monaco, text_name, text_name + ".txt")
                            text = open(path).read()
                            texts.append(text)
                            text_names.append(text_name)
                            n = texts_sents[text_name]
                            max_sents = max(max_sents, n - (n % 100) + (0 if n % 100 == 0 else 100))
                            break
        
        for label_condition in label_conditions:

            argx = args[:]
            argx[3] = label_condition
            results_file_name = "_".join(argx[1:]) + ".txt"
            results_file_path = os.path.join("results", results_file_name)

            if not os.path.exists(results_file_path):
                
                if len(text_names) != len(test_texts):
                    raise ValueError("Not all test texts found!", "/", "Given:", test_texts, "/", "Found:", text_names)

                nlp = build_pipeline(max_sents, gen_tagger, label_condition, context_tokens, hyperparameter_setup, cv, sitent_data)
                docs = [nlp(text) for text in texts]
                sents = [list(doc.sents) for doc in docs]

                if sitent_data is None:
                    texts_sents = TEXTS_SENTS.copy()
                    texts_sents.update(TEXTS_SENTS_2)
                    end_of_annotations = [sents[n][texts_sents[text_name]-1].end-1 for n, text_name in enumerate(text_names)]
                else:
                    end_of_annotations = [len(doc)-1 for doc in docs]

                precisions, recalls, fscores, confusions = get_scores(docs, end_of_annotations, label_condition)

                if label_condition == "binary":
                    binary_precisions, binary_recalls, binary_fscores = precisions, recalls, fscores
                else:
                    binary_precisions, binary_recalls, binary_fscores, _ = get_scores(docs, end_of_annotations, "binary")
                precisions["binar"] = binary_precisions["GI"]
                recalls["binar"] = binary_recalls["GI"]
                fscores["binar"] = binary_fscores["GI"]

                original_stdout = sys.stdout
                with open(results_file_path, "w") as f:
                    sys.stdout = f
                    print(pd.DataFrame({"P" : precisions, "R" : recalls, "F" : fscores}))
                    print()
                    print(confusion_matrix(confusions))
                sys.stdout = original_stdout
            
            with open(results_file_path, "r") as f:
                print(f.read())