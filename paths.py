import os

PATH_TO_MONACO = os.path.join(os.path.dirname(__file__), "..", "korpus-public-v4.1")
PATH_TO_MONACO_2 = os.path.join(os.path.dirname(__file__), "..", "korpus-public-v5.0")
PATH_TO_MONAPIPE = os.path.join(os.path.dirname(__file__), "..", "pipy-public-v3.2")
PATH_TO_SITENT = os.path.join(os.path.dirname(__file__), "..", "SitEnt_DE")
