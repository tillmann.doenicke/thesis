import csv
import itertools
import numpy as np
import os
import pickle
import sys
import tqdm
from multiprocessing import Pool
from scipy.stats import spearmanr

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from measures import *
from texts import TEXTS_SENTS


class Sample():
    """Class to store a sample.

    """
    def __init__(self, sample):
        """`__init__` method of class `Sample`.

        Args:
            sample (dict of str:str): The sample as feature-value dict.
        
        """
        # text, sent_id, clause_id:
        self.text = sample["text"]
        self.sent_id = sample["sent_id"]
        self.clause_id = sample["clause_id"]
        # tags y (str) and gi (boolean):
        self.y = sample["p-tag"]
        self.gi = (self.y != "NONE")
        # texts (str) for "p", "c", "R", "Q", "N":
        self.texts = {}
        # lemmas (str) for "Q", "N":
        self.lemmas = {}
        # counts (float) for "tokens", "clauses", "sentences":
        self.counts = {}
        # percentages (float) for "attr1", "attr2", "attr3", "attr4":
        self.attrs = {}
        # overlaps (boolean) for "comment", "nfr":
        self.overlaps = {}
        # feature combinations (str):
        self.fs = set()
        for f_name in sample:
            f_val = sample[f_name]
            if f_name in ["p", "c", "R", "Q", "N"]:
                self.texts[f_name] = f_val
            elif f_val == "":
                continue
            elif f_name.endswith("-lemma"):
                self.lemmas[f_name.split("-")[0]] = f_val
            elif f_name[:2] in ["c-", "R-", "Q-", "N-"] or f_name == "QRN":
                self.fs.add(f_name + "=" + f_val)
            elif f_name.startswith("p-#"):
                self.counts[f_name[3:]] = float(f_val)
            elif f_name.startswith("p-attr"):
                self.attrs[f_name[2:]] = float(f_val)
            elif f_name.startswith("p-") and f_name != "p-tag":
                self.overlaps[f_name[2:]] = (f_val == "True")
        self.fs_valued = set([f.split("=")[0] for f in self.fs])
        self.fs = fs_combinations(self.fs, 3)
    
    def check(self, f):
        """Checks whether the features of a feature combination are valued in a sample.

        Args:
            f (str): A feature combination.
        
        Returns:
            boolean: Whether all features of the combination are valued in the sample.
        
        """
        for f in f.split("&"):
            if f.split("=")[0] not in self.fs_valued:
                return False
        return True
    
    def __str__(self):
        dd = vars(self)
        del dd["fs"]
        return str(dd)


def fs_combinations(fs, n):
    """Create feature combinations.

    Args:
        fs (set of str): Atomic features.
        n (int): Maximum size of feature combinations.
    
    Returns:
        set of str: Feature combinations.
            Features are concatenated by "&".
    
    """
    fs = sorted(list(fs))
    fs_combs = []
    for r in range(1, n+1):
        combs = list(itertools.combinations(fs, r))
        combs = ["&".join(comb) for comb in combs]
        fs_combs.extend(combs)
    return set(fs_combs)


def read_data():
    """Read the data from `data.csv`.

    Returns:
        list of `Sample`: List of samples.
    
    """
    samples = []
    with open(os.path.join(os.path.dirname(__file__), "data.csv"), "r") as f:
        csv_reader = list(csv.reader(f, delimiter=","))
        for row in csv_reader[1:]:
            sample = {key : row[i] for i, key in enumerate(csv_reader[0])}
            samples.append(Sample(sample))
    return samples


def stats_freq(X, per):
    """TODO
    """
    freqs = {"" : 0}
    for x in X:
        if x.gi:
            try:
                freqs[getattr(x, per)] += 1
            except KeyError:
                freqs[getattr(x, per)] = 1
            freqs[""] += 1
    return freqs


def stats_length(X, per):
    """TODO
    """
    counts = {"tokens" : {"" : []}, "clauses" : {"" : []}, "sentences" : {"" : []}}
    for x in X:
        if x.gi:
            for unit in x.counts:
                try:
                    counts[unit][getattr(x, per)].append(x.counts[unit])
                except KeyError:
                    counts[unit][getattr(x, per)] = [x.counts[unit]]
                counts[unit][""].append(x.counts[unit])
    return {unit : {per : (np.mean(counts[unit][per]), np.std(counts[unit][per])) for per in counts[unit]} for unit in counts}


def format_row(vals, eol=r"\\", ct="al"):
    """TODO
    """
    if type(vals) in [list, tuple]:
        return " & ".join([format_row(val, "", ct) for val in vals]) + eol
    if type(vals) == str:
        if "__" in vals:
            ab = vals.split("__")
            a = ab[0].lower().replace("ä", "a").replace("ö", "o").replace("ü", "u")
            b = ab[1].lower().replace("ä", "ae").replace("ö", "oe").replace("ü", "ue").split("_")
            b = [x for x in b if x not in ["der", "die", "das"]][0]
            return r"\cite" + ct + r"pshort{" + a + ":" + b + r"}"
        vals = vals.replace("&", r"{\land}")
        if "==>" in vals:
            vals = vals.replace("==>", r"{\implies}")
            vals = vals.replace("!", r"{\lnot}")
        return vals
    if type(vals) == int:
        return "$" + str(vals) + "$"
    return "$" + "{:.2f}".format(vals).lstrip("0").replace("-0.", "-.") + "$"


if __name__ == "__main__":
    args = sys.argv

    X = read_data()

    # number and length of GI passages per text
    if "gi_per_text" in args:
        TEXTS_SENTS[""] = sum(TEXTS_SENTS.values())
        text_freq = stats_freq(X, "text")
        text_length = stats_length(X, "text")
        texts = sorted(text_freq.keys(), key=lambda text: 1.0*text_freq[text]/TEXTS_SENTS[text], reverse=True)
        avg_n = []
        avg_r = []
        len_t = []
        len_c = []
        len_s = []
        for text in texts:
            vals = [text_freq[text], 1.0*text_freq[text]/TEXTS_SENTS[text], text_length["tokens"][text], text_length["clauses"][text], text_length["sentences"][text]]
            print(format_row([text] + vals))
            avg_n.append(vals[0])
            avg_r.append(vals[1])
            len_t.append(vals[2][0])
            len_c.append(vals[3][0])
            len_s.append(vals[4][0])
        print(format_row(["AVERAGE", np.mean(avg_n), np.mean(avg_r), np.mean(len_t), np.std(len_t), np.mean(len_c), np.std(len_c), np.mean(len_s), np.std(len_s)]))
        print()

    # number and length of GI passages per tag
    if "gi_per_tag" in args:
        y_freq = stats_freq(X, "y")
        y_length = stats_length(X, "y")
        ys = ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG", ""]
        for y in ys:
            print(format_row(["\\" + y.lower(), y_freq[y], y_length["tokens"][y], y_length["clauses"][y], y_length["sentences"][y]]))
        print()

    # frequency of quantifiers per tag
    if "q_per_tag" in args:
        lemma_counts = {}
        lemma_examples = {}
        for x in X:
            if not (x.gi and "QN" in x.lemmas):
                continue
            if x.y not in lemma_counts:
                lemma_counts[x.y] = {}
                lemma_examples[x.y] = {}
            lemma = x.lemmas["QN"].replace(" ... ", " ").replace(" ", " $+$ ")
            if lemma not in lemma_counts[x.y]:
                lemma_counts[x.y][lemma] = {}
                lemma_examples[x.y][lemma] = {}
            q_pos = "NONE"
            for f in sorted(list(x.fs)):
                if f.startswith("Q-pos="):
                    q_pos = f.split("=")[1]
                    break
            if q_pos not in lemma_counts[x.y][lemma]:
                lemma_counts[x.y][lemma][q_pos] = 0
                lemma_examples[x.y][lemma][q_pos] = []
            lemma_counts[x.y][lemma][q_pos] += 1
            lemma_examples[x.y][lemma][q_pos].append((x.texts["p"], x.text))
        
        ys = ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"]
        for y in ys:
            print(y)
            lemmas = sorted(lemma_counts[y].keys(), key=lambda lemma: sum(lemma_counts[y][lemma].values()), reverse=True)
            for lemma in lemmas:
                q_poss = sorted(lemma_counts[y][lemma].keys(), key=lambda q_pos: lemma_counts[y][lemma][q_pos], reverse=True)
                for q_pos in q_poss:
                    examples = sorted(lemma_examples[y][lemma][q_pos], key=lambda example: len(example[0]))
                    print(format_row([r"\textit{" + lemma + r"}", r"\upos{" + q_pos + r"}", lemma_counts[y][lemma][q_pos]]))
                    print("%", examples[:5])
        print()

    # correlations of GI and features
    if "gi_vs_f" in args:
        fs_ = set([f for x in X for f in x.fs])
        print(len(fs_))
        y_ = ["GI", "ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"]
        all_results = {}
        for n in range(1):
            fs = [f for f in fs_ if f.count("&") == n]
            for exclude_other_subtags in [False, True, None]:
                all_results[exclude_other_subtags] = {}
                for y in y_:
                    all_results[exclude_other_subtags][y] = []
                    path_vars = [str(exclude_other_subtags), y, str(n+1)]
                    #if y == "GI":
                    #    path_vars[0] = str(False)
                    pickle_path = os.path.join(os.path.dirname(__file__), "pickles", "_".join(path_vars) + ".pickle")
                    if os.path.exists(pickle_path):
                        with open(pickle_path, "rb") as f:
                            results = pickle.load(f)
                    else:
                        X_y = X
                        y_y = y
                        if y != "GI":
                            if exclude_other_subtags is None:
                                X_y = [x for x in X if "Q-type=" + y in x.fs]
                                y_y = "GI"
                            elif exclude_other_subtags:
                                X_y = [x for x in X if x.y in [y, "NONE"]]
                        results = []
                        for f in tqdm.tqdm(fs):
                            results.append(all_measures(X_y, y_y, f))
                        with open(pickle_path, "wb") as f:
                            pickle.dump(results, f)
                    all_results[exclude_other_subtags][y].extend(results)
        for exclude_other_subtags in [False, True, None]:
            for y in y_:
                results = all_results[exclude_other_subtags][y]
                results = filter(lambda res: res[3] is not None, results)
                results = filter(lambda res: res[3] >= 0.1, results)
                #results = filter(lambda res: res[5] == "*", results)
                results = sorted(results, key=lambda res: res[3], reverse=True)
                print(exclude_other_subtags, y)
                X_y = X
                y_y = y
                if y != "GI":
                    if exclude_other_subtags is None:
                        X_y = [x for x in X if "Q-type=" + y in x.fs]
                        y_y = "GI"
                    elif exclude_other_subtags:
                        X_y = [x for x in X if x.y in [y, "NONE"]]
                for _, f, n, v, x2, significance, p, direction in results:
                    n_p = 1.0*n/len(X)
                    X_y_f = [x for x in X_y if x.check(f)]
                    try:
                        eps = count(X_y_f, y_y, f)-calc(X_y_f, y_y, f)
                    except ZeroDivisionError:
                        continue
                    polarity = ""
                    if eps < 0:
                        polarity = "!"
                        p = P_D(X_y_f, y_y, not_(f))
                    if p < 0:
                        direction = polarity + "f ==> y"
                    else:
                        direction = "y ==> " + polarity + "f"
                    yy = y_y
                    if y_y != y:
                        yy = y_y + "|" + y
                    print(format_row([yy, f, n, n_p, v, x2, significance, eps, p, direction]))
    
    # GI passages per Q-type
    if "gi_per_q" in args:
        ys = ["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"]
        counts = {y : {True : 0, False : 0} for y in ys+["NONE", ""]}
        for x in X:
            notype = True
            for y in ys:
                if has_f(x, "Q-type=" + y):
                    counts[y][x.gi] += 1
                    notype = False
                    break
            if notype:
                counts["NONE"][x.gi] += 1
                print(x)
            counts[""][x.gi] += 1
        for y in ys+["NONE", ""]:
            total = 1.0*sum(counts[y].values())
            print(format_row([y, counts[y][True], counts[y][True]/total, counts[y][False], counts[y][False]/total]))