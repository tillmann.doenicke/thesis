import os
import pandas as pd
import re
import shutil
import sys
from collections import OrderedDict

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from nlp import NLP
from paths import PATH_TO_MONACO, PATH_TO_MONAPIPE
from q_parsing import *
from texts import TEXTS_SENTS

sys.path.append(PATH_TO_MONAPIPE)
from pipeline.annotation import Annotation
from pipeline.global_resources import GERMANET
from pipeline.utils_classes import Morph
from pipeline.utils_methods import get_subj_i


# feature value for empty words; note the difference:
# "NONE" (feature has the value EMPTY_WORD) vs. `None` (feature has no value)
EMPTY_WORD = "NONE"


def corrected_referent(tokens):
    """Make some corrections to the annotated key referent.
        1. If the key referent spans 0 tokens, return None; otherwise return the first non-punctuation non-space token.
        2. If a (semi-)modal verb was annotated; move the annotation to the corresponding main verb.
    
    Args:
        tokens (list of `Token`): The annotated key referent as a list of tokens.
    
    Returns:
        `Token`: The key referent.
    
    """
    referent = None
    tokens = [token for token in tokens if not (token.is_punct or token.is_space)]
    if len(tokens) > 0:
        referent = tokens[0]
        if referent._.clause._.form is not None:
            if referent in referent._.clause._.form.modals:
                referent = referent._.clause._.form.main
    return referent


def get_attribution(annotation):
    """Get attribution annotations.

    Args:
        annotation (`Annotation`): An annotation.
    
    Returns:
        float: Percentage of character speech.
        float: Percentage of narrator speech.
        float: Percentage of author speech.
        float: Percentage of work-external speech.
    
    """
    attr1, attr2, attr3, attr4 = 0, 0, 0, 0
    attr = False
    for clause in annotation.clauses:
        count_attr1 = [0, 0, 0, 0, 0, 0]
        count_attr2 = [0, 0, 0, 0, 0, 0]
        count_attr3 = [0, 0, 0, 0, 0, 0]
        count_attr4 = [0, 0, 0, 0, 0, 0]
        for a, annotator in enumerate(["A1", "A2", "A3", "A4", "A5", "A6"]):
            for attribution in clause._.annotations[annotator].get_annotations(tags=["Attribution"], tagset="Attribution IV"):
                props = attribution.property_values
                if "Attribution" in props:
                    vals = props["Attribution"]
                    if "Figur" in vals:
                        count_attr1[a] = 1
                    if "Erzählinstanz" in vals:
                        count_attr2[a] = 1
                    if "Verdacht Autor" in vals:
                        count_attr3[a] = 1
                    if "Werkexterne Instanz" in vals:
                        count_attr4[a] = 1
                attr = True
        attr1 += sum(count_attr1)
        attr2 += sum(count_attr2)
        attr3 += sum(count_attr3)
        attr4 += sum(count_attr4)
    if attr:
        return [a/(6.0*len(annotation.clauses)) for a in [attr1, attr2, attr3, attr4]]
    else:
        return [None for a in [attr1, attr2, attr3, attr4]]


def add_morph_feats(morph_feats, token, with_children=True):
    """Updates a morphological feature dictionary.

    Args:
        morph_feats (dict of str:(set of str)): Dictionary with morphological features, e.g. "case_" : {"nom"}.
        token (`Token`): The token whose features should be added to the dictionary.
        with_children (boolean): Iff True, also the features of the token's children are added.
    
    """
    token_morph_feats = token._.morph.as_dict()
    token_morph_feats["additional_attributes_"] = set()
    if phrase_type(token) == "NP":
        if token.pos_ == "DET" and not (token.lemma_.lower() in QUANTIFIER_LEMMAS or token.lemma_.lower() in NEGATOR_LEMMAS or token.dep_.split(":")[0] == "nummod"):
            token_morph_feats["additional_attributes_"].add(token.lemma_.lower())
        if with_children:
            for child in token.children:
                add_morph_feats(token_morph_feats, child, False)
    for feat in token_morph_feats:
        try:
            morph_feats[feat].update(token_morph_feats[feat])
        except KeyError:
            morph_feats[feat] = set(token_morph_feats[feat])


def code_lemma(q_word, referent, negator, tag):
    """Return the ordered lemma of Q-word and negator.

    Args:
        q_word (`Token`): Q-word.
        referent (`Token`): Key referent.
        negator (`Token`): Negator.
    
    Returns:
        str: The lemma construction as string.
    
    """
    q_lemma = ((None if tag in ["ALL", "DIV", "EXIST", "MEIST"] else EMPTY_WORD) if q_word is None else q_word.lemma_.lower())
    n_lemma = (EMPTY_WORD if negator is None else negator.lemma_.lower())
    if q_lemma is None:
        return None
    if negator is None:
        return q_lemma
    if q_word is None:
        if referent is None:
            return None
        d = negator.i-referent.i
    else:
        d = negator.i-q_word.i
    if d < 0:
        return n_lemma + (" ... " if abs(d) > 1 else " ") + q_lemma
    else:
        return q_lemma + (" ... " if abs(d) > 1 else " ") + n_lemma


def code_construction(q_word, referent, negator):
    """Create a unique string representation of a Q-word construction.

    Args:
        q_word (`Token`): Q-word.
        referent (`Token`): Key referent.
        negator (`Token`): Negator.
    
    Returns:
        str: The Q-word construction as string.
    
    """
    tokens = sorted([token for token in [q_word, referent, negator] if token is not None], key=lambda token: token.i)
    if len(tokens) == 0:
        return None
    code = []
    for token in tokens:
        char = None
        if token == q_word:
            char = "Q"
        elif token == referent:
            char = "R"
        elif token == negator:
            char = "N"
        pos = token.pos_
        dep = token.dep_.split(":")[0]
        if token.head in tokens:
            dist = tokens.index(token.head)-tokens.index(token)
            dist = ("+" + str(dist) if dist > 0 else str(dist))
        else:
            dist = "*"
        code.append(".".join([char, pos, dep, dist]))
    return "-".join(code)


# True: only process texts with MONAPipe; False: also create data
pipe_only = False

if __name__ == "__main__":
    GERMANET.provide()

    # iterate over texts; create samples
    samples = []
    for text in sorted(TEXTS_SENTS.keys()):
        path = os.path.join(PATH_TO_MONACO, text)
        if os.path.isdir(path):
            print(text)

            # copy the RRR file to the corresponding MONACO directory
            shutil.copyfile(os.path.join(os.path.dirname(__file__), "..", "key_referents", text, "RRR.xml"), os.path.join(PATH_TO_MONACO, text, "RRR.xml"))
            
            with open(os.path.join(path, text + ".txt")) as f:
                
                # pipe the text through MONAPipe
                doc = NLP(f.read())
                sents = list(doc.sents)
                
                if not pipe_only:
                    
                    # last annotated sentence and clause (end of the annotation)
                    last_annotated_sent = sents[TEXTS_SENTS[text]-1]

                    # GI annotations
                    annotations = doc._.annotations["GGG"].get_annotations(tagset="Reflexion IV", tags=["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"])
                    annotations = [annotation for annotation in annotations if annotation.tokens[-1].i < last_annotated_sent.end]

                    # predicted clauses that are part of GI annotations
                    annotated_clauses = set([clause for annotation in annotations for clause in annotation.clauses])
                    
                    # add non-GI "annotations" (clauses that are not part of GI annotations)
                    for sent in sents[:sents.index(last_annotated_sent)]:
                        for clause in sent._.clauses:
                            if clause not in annotated_clauses:
                                annotations.append(Annotation("NONE", "Reflexion IV", {}, clause._.tokens, [clause], None, None, None))
                    
                    # remove ambiguous passages
                    annotations = [annotation for annotation in annotations if not ("Sicherheit" in annotation.property_values and len([item for item in annotation.property_values["Sicherheit"] if "ambig" in item]) > 0)]
                    
                    # sort annotations (only for convenience)
                    annotations = sorted(annotations, key=lambda annotation: annotation.tokens[0].i)
                    
                    # annotations of key referents
                    annotated_referents = set([corrected_referent(annotation.tokens) for annotation in doc._.annotations["RRR"]])
                    annotated_referents.discard(None)
                    
                    # iterate over annotations; create one sample per annotation
                    for annotation in annotations:
                        
                        # compute for every (non-punctuation, non-space) token in the annotation with how many (GI) annotations it overlaps;
                        # only take the tokens with a minimal number of annotations (i.e. "outer tokens") for feature extraction
                        # (the other tokens (i.e. "inner tokens") are included in other annotations)
                        tokens = [token for token in annotation.tokens if not (token.is_punct or token.is_space)]
                        overlaps = [len(token._.annotations["GGG"].get_annotations(tagset="Reflexion IV", tags=["ALL", "BARE", "DIV", "EXIST", "MEIST", "NEG"])) for token in tokens]
                        tokens = [token for n, token in enumerate(tokens) if overlaps[n] == min(overlaps)]

                        # Q-parsing
                        try:
                            clause, referent, q_word, negator = parse(tokens, annotation.tag, annotated_referents)
                            parsed = True
                        except Exception as e:
                            print(e)
                            clause, referent, q_word, negator = None, None, None, None
                            parsed = False

                        # if Q-parsing returned None for all variables, select the first clause from the annotation;
                        # skip annotations that do not contain a (predicted) clause
                        if clause is None and len(annotation.clauses) > 0:
                            clause = annotation.clauses[0]
                        
                        # attribution labels
                        attr1, attr2, attr3, attr4 = get_attribution(annotation)

                        # create sample; add features for passage
                        sample = OrderedDict()
                        sample["text"] = text
                        sample["sent_id"] = (sents.index(annotation.tokens[0].sent) if clause is None else sents.index(clause.root.sent))+1
                        sample["clause_id"] = (None if clause is None else clause.root.sent._.clauses.index(clause)+1)
                        sample["p"] = re.sub(r"\s+", " ", "".join([token.text_with_ws for token in annotation.tokens]))
                        sample["p-tag"] = annotation.tag
                        sample["p-#tokens"] = len(annotation.tokens)
                        sample["p-#clauses"] = len(annotation.clauses)
                        sample["p-#sentences"] = sum([1.0/len(_clause.root.sent._.clauses) for _clause in annotation.clauses])
                        sample["p-comment"] = (sum([len(_clause._.annotations["GGG"].get_annotations(tagset="Reflexion IV", tags=["Einstellung", "Interpretation", "Meta"])) for _clause in annotation.clauses]) > 0)
                        sample["p-nfr"] = (sum([len(_clause._.annotations["GGG"].get_annotations(tagset="Reflexion IV", tags=["Nichtfiktional"])) for _clause in annotation.clauses]) > 0)
                        sample["p-nfr+mk"] = (sum([len(_clause._.annotations["GGG"].get_annotations(tagset="Reflexion IV", tags=["Nichtfiktional", "Nichtfiktional+mK"])) for _clause in annotation.clauses]) > 0)
                        sample["p-attr1"] = attr1
                        sample["p-attr2"] = attr2
                        sample["p-attr3"] = attr3
                        sample["p-attr4"] = attr4

                        # it can happen that the clause returned by Q-parsing is not among the (predicted) clauses of the annotation
                        # (because of parsing errors); in this case, no features from the clause are extracted
                        if clause in annotation.clauses:
                        
                            # word order
                            wo = word_order(clause)

                            # clausal features
                            form = clause._.form
                            
                            # add features for key clause
                            sample["c"] = re.sub(r"\s+", " ", " ".join([token.text for token in clause._.tokens]))
                            sample["c-pos"] = clause.root.pos_
                            sample["c-dep"] = clause.root.dep_.split(":")[0]
                            sample["c-order1"] = wo[0]
                            sample["c-order2"] = wo[1]
                            sample["c-subordination"] = (len([_clause for _clause in clause.root.sent._.clauses if _clause != clause and _clause.root.dep_.split(":")[0] not in ["conj", "parataxis", "list"] and _clause.root in clause.subtree]) > 0)
                            sample["c-form"] = (None if form is None else form.verb_form)
                            sample["c-tense"] = (None if form is None else form.tense)
                            sample["c-aspect"] = (None if form is None else form.aspect)
                            sample["c-mood"] = (None if form is None else form.mode)
                            sample["c-voice"] = (None if form is None or form.voice is None else form.voice.split(":")[0])
                            sample["c-static"] = (None if form is None or form.voice is None or len(form.voice.split(":")) == 1 else form.voice.split(":")[1])
                            sample["c-modal"] = (None if form is None else (EMPTY_WORD if len(form.modals) == 0 else sorted(form.modals, key=lambda token: root_distance(token))[0].lemma_))
                            sample["c-class"] = (None if clause._.verb_synset_id is None else str(GERMANET._.get_synset_by_id(clause._.verb_synset_id).word_class).replace("WordClass.", ""))
                            sample["c-event"] = (None if clause._.event is None else clause._.event["event_types"])
                            sample["c-speech"] = (None if len(clause._.speech) == 0 else sorted(sorted(clause._.speech.keys()), key=lambda speech_type: -clause._.speech[speech_type])[0])
                        
                        # only if Q-parsing was successful ...
                        if parsed:
                            
                            # referent type
                            referent_type = (None if referent is None else phrase_type(referent))

                            # morphological features
                            # if the referent is an NP, the features are those of the NP + those of the NP's children
                            # if the referent is a VP, the features are those of the VP + those of the VP's subject + those of the subject's children
                            morph_feats = {}
                            if referent_type is not None:
                                add_morph_feats(morph_feats, referent)
                                if referent_type == "VP":
                                    subj_i = get_subj_i(referent)
                                    if subj_i > -1:
                                        subj = doc[subj_i]
                                        add_morph_feats(morph_feats, subj)
                            morph = Morph({}, {feat[:-1] : morph_feats[feat] for feat in morph_feats})
                            
                            # add features for key referent, Q-word and negator
                            sample["R"] = ("" if referent is None else referent.text)
                            sample["R-pos"] = (None if referent is None else referent.pos_)
                            sample["R-dep"] = (None if referent is None else referent.dep_.split(":")[0])
                            sample["R-type"] = referent_type
                            sample["R-case"] = (None if morph is None else morph.case)
                            sample["R-person"] = (None if morph is None else morph.person)
                            sample["R-number"] = (None if morph is None else morph.numerus)
                            sample["R-gender"] = (None if morph is None or morph.gender == "noGender" else morph.gender)
                            sample["R-det"] = (None if morph is None else (EMPTY_WORD if len(morph.additional_attributes_) == 0 else morph.additional_attributes))
                            sample["Q"] = ("" if q_word is None else q_word.text)
                            sample["Q-pos"] = (None if q_word is None else q_word.pos_)
                            sample["Q-dep"] = (None if q_word is None else q_word.dep_.split(":")[0])
                            sample["Q-lemma"] = ((None if annotation.tag in ["ALL", "DIV", "EXIST", "MEIST"] else EMPTY_WORD) if q_word is None else q_word.lemma_.lower())
                            sample["Q-type"] = ((None if annotation.tag in ["ALL", "DIV", "EXIST", "MEIST"] else "BARE") if q_word is None else QUANTIFIER_LEMMAS[q_word.lemma_.lower()])
                            sample["N"] = ("" if negator is None else negator.text)
                            sample["N-pos"] = (None if negator is None else negator.pos_)
                            sample["N-dep"] = (None if negator is None else negator.dep_.split(":")[0])
                            sample["N-lemma"] = (EMPTY_WORD if negator is None else negator.lemma_.lower())
                            sample["QN-lemma"] = code_lemma(q_word, referent, negator, annotation.tag)
                            sample["QRN"] = code_construction(q_word, referent, negator)
                        
                        samples.append(sample)
            
            # remove the RRR file from the corresponding MONACO directory
            os.remove(os.path.join(PATH_TO_MONACO, text, "RRR.xml"))

    # fill missing values with `None`
    keys = sorted(samples, key=lambda sample: len(sample.keys()))[-1].keys()
    new_samples = []
    for sample in samples:
        new_sample = OrderedDict()
        for key in keys:
            new_sample[key] = (sample[key] if key in sample else None)
        new_samples.append(new_sample)
    samples = new_samples    

    if not pipe_only:
        # write data to file
        df = pd.DataFrame.from_dict(samples)
        df.to_csv(os.path.join(os.path.dirname(__file__), "data.csv"), index=False, header=True)
        df.transpose().to_csv(os.path.join(os.path.dirname(__file__), "data_transposed.csv"), index=True, header=False)