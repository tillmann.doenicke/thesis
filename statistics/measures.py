from math import sqrt


def not_(f):
    """Negates a feature.
        Example:
            "c-tense=pres" -> "!c-tense=pres"
            "!c-tense=pres" -> "c-tense=pres"

    Args:
        f (str): Feature.
    
    Returns:
        str: Negated feature.
    
    """
    return (f[1:] if f.startswith("!") else "!" + f)


def has_y(x, y):
    """Checks whether a sample has a given tag.

    Args:
        x (`Sample`): Sample.
        y (str): Tag.
    
    Returns:
        boolean: Whether the sample has the tag.
    
    """
    if y is None:
        return True
    y_pos = True
    if y.startswith("!"):
        y_pos = False
        y = y[1:]
    if y == "GI":
        return (x.gi == y_pos)
    else:
        return ((x.y == y) == y_pos)


def has_f(x, f):
    """Checks whether a sample has a given feature.

    Args:
        x (`Sample`): Sample.
        f (str): Feature.
    
    Returns:
        boolean: Whether the sample has the feature.
    
    """
    if f is None:
        return True
    f_pos = True
    if f.startswith("!"):
        f_pos = False
        f = f[1:]
    return ((f in x.fs) == f_pos)


def count(X, y, f):
    """Counts the samples that have a given tag and a given feature.
        If `y` or `f` is `None`, all samples are treated as having it.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        float: Number of samples that have the tag and the feature.
    
    """
    s = 0.0
    for x in X:
        if has_y(x, y) and has_f(x, f):
            s += 1.0
    return s


def calc(X, y, f):
    """Estimates the samples that have a given tag and a given feature.
        If `y` or `f` is `None`, all samples are treated as having it.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        float: Estimated number of samples that have the tag and the feature.
    
    """
    s_y = count(X, y, None)
    s_f = count(X, None, f)
    return s_y*s_f/len(X)


def X_squared(X, y, f):
    """Returns the chi-squared measure for a tag-feature combination.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        float: Chi-squared measure.
    
    """
    s = 0.0
    for y_ in [y, not_(y)]:
        for f_ in [f, not_(f)]:
            try:
                c1 = count(X, y_, f_)
                c2 = calc(X, y_, f_)
                s += 1.0*((c1-c2)**2)/c2
            except ZeroDivisionError:
                return None
    return s


def V(X, y, f, x2=None):
    """Returns Cramer's correlations coefficient for a tag-feature combination.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        float: Cramer's correlation coefficient.
    
    """
    return sqrt(X_squared(X, y, f)/len(X))


def P_D(X, y, f):
    """Returns the difference in conditional probability for a tag-feature combination.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        float: Difference in conditional probability.
    
    """
    c = count(X, y, f)
    try:
        return c/count(X, y, None)-c/count(X, None, f)
    except ZeroDivisionError:
        return None


def all_measures(X, y, f):
    """Returns various measures for a tag-feature combination.

    Args:
        X (list of `Sample`): Samples.
        y (str): Tag.
        f (str): Feature.
    
    Returns:
        int: Number of samples on which the measures are computed/computable.
        float: Cramer's correlation coefficient.
        float: Chi-squared measure.
        str: Significance ("*": significant; " ": not significant)
        float: Difference in conditional probability.
        str: Implicational formula.
    
    """
    X = [x for x in X if x.check(f)]
    x2 = X_squared(X, y, f)
    if x2 is None:
        v = None
        significance = None
    else:
        v = sqrt(x2/len(X))
        significance = ("*" if x2 > 3.84 else " ")
    p = P_D(X, y, f)
    if p is None:
        direction = None
    else:
        if p <= -0.33:
            direction = str(f) + " ==> " + y
        elif p > 0.33:
            direction = y + " ==> " + str(f)
        else:
            direction = y + " <=> " + str(f)
    return y, f, len(X), v, x2, significance, p, direction