import os
import spacy
import sys

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from paths import PATH_TO_MONACO, PATH_TO_MONAPIPE

sys.path.append(PATH_TO_MONAPIPE)
from pipeline.components.analyzer import demorphy_analyzer
from pipeline.components.annotation_reader_catma import annotation_reader_catma
from pipeline.components.clausizer import dependency_clausizer
from pipeline.components.event_tagger import event_event_tagger
from pipeline.components.germanet_tagger import germanet_tagger_verbs_adjectives
from pipeline.components.lemma_fixer import lemma_fixer
from pipeline.components.normalizer import dictionary_normalizer
from pipeline.components.pipeline_pickler import pickle_init, pickle_wrapper
from pipeline.components.sentencizer import spacy_sentencizer
from pipeline.components.slicer import max_sent_slicer
from pipeline.components.speech_tagger import flair_speech_tagger
from pipeline.components.tense_tagger import rb_tense_tagger
from pipeline.global_constants import SPACY_MAX_LENGTH
from settings import PARSING_PATH, PICKLE_PATH


model = os.path.join(PARSING_PATH, "de_ud_lg")

NLP = spacy.load(model, disable=["ner"])
NLP.max_length = SPACY_MAX_LENGTH

def pw(doc, func, **kwargs):
    return pickle_wrapper(doc, func, load_output=True, save_output=True, overwrite=False, pickle_path=PICKLE_PATH, **kwargs)

pickle_init_ = lambda doc: pickle_init(doc, model=os.path.basename(model), slicer="max_sent_slicer", max_units=700)
NLP.add_pipe(pickle_init_, name="pickle_init", before="tagger")

dictionary_normalizer_ = lambda doc: pw(doc, dictionary_normalizer)
NLP.add_pipe(dictionary_normalizer_, name="normalizer", before="tagger")

lemma_fixer_ = lambda doc: pw(doc, lemma_fixer)
NLP.add_pipe(lemma_fixer_, name="lemma_fixer", after="tagger")

spacy_sentencizer_ = lambda doc: pw(doc, spacy_sentencizer)
NLP.add_pipe(spacy_sentencizer_, name="sentencizer", before="parser")

max_sent_slicer_ = lambda doc: pw(doc, max_sent_slicer)
NLP.add_pipe(max_sent_slicer_, name="slicer", after="parser")

demorphy_analyzer_ = lambda doc: pw(doc, demorphy_analyzer)
NLP.add_pipe(demorphy_analyzer_, name="analyzer")

dependency_clausizer_ = lambda doc: pw(doc, dependency_clausizer)
NLP.add_pipe(dependency_clausizer_, name="clausizer")

rb_tense_tagger_ = lambda doc: pw(doc, rb_tense_tagger)
NLP.add_pipe(rb_tense_tagger_, name="tense_tagger")

event_event_tagger_ = lambda doc: pw(doc, event_event_tagger)
NLP.add_pipe(event_event_tagger_, name="event_tagger")

flair_speech_tagger_ = lambda doc: pw(doc, flair_speech_tagger)
NLP.add_pipe(flair_speech_tagger_, name="speech_tagger")

germanet_tagger_verbs_adjectives_ = lambda doc: pw(doc, germanet_tagger_verbs_adjectives)
NLP.add_pipe(germanet_tagger_verbs_adjectives_, name="germanet_tagger")

annotation_reader_catma_ = lambda doc: annotation_reader_catma(doc, corpus_path=PATH_TO_MONACO)
NLP.add_pipe(annotation_reader_catma_, name="annotation_reader")