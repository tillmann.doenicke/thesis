import numpy as np
import os
import sys
from scipy.stats import spearmanr

from correlations import *
from measures import *

sys.path.append(os.path.join(os.path.dirname(__file__), ".."))
from nlp import NLP, pw
from paths import PATH_TO_MONACO, PATH_TO_MONAPIPE
from texts import TEXTS_SENTS

sys.path.append(PATH_TO_MONAPIPE)
from pipeline.components.attribution_tagger import neural_attribution_tagger


for pipe_name in ['tense_tagger', 'event_tagger', 'speech_tagger', 'germanet_tagger']:
    NLP.remove_pipe(pipe_name)

neural_attribution_tagger_ = lambda doc: pw(doc, neural_attribution_tagger)
NLP.add_pipe(neural_attribution_tagger_, name="attribution_tagger", before="annotation_reader")


if __name__ == "__main__":

    # iterate over texts; create samples
    X = []
    for text in sorted(TEXTS_SENTS.keys()):
        path = os.path.join(PATH_TO_MONACO, text)
        if os.path.isdir(path):
            print(text)

            with open(os.path.join(path, text + ".txt")) as f:
                
                # pipe the text through MONAPipe
                doc = NLP(f.read())
                sents = list(doc.sents)

                # last annotated sentence and clause (end of the annotation)
                last_annotated_sent = sents[TEXTS_SENTS[text]-1]
                
                for j, sent in enumerate(sents):
                    for k, clause in enumerate(sent._.clauses):
                        sample = {}
                        sample["text"] = text
                        sample["sent_id"] = (j+1)
                        sample["clause_id"] = (k+1)
                        tags = clause._.annotations["GGG"].get_tags(tagset="Reflexion IV")
                        sample["p-tag"] = "NONE"
                        for tag in ["ALL", "BARE", "MEIST", "DIV", "EXIST", "NEG"]:
                            if tag in tags:
                                sample["p-tag"] = tag
                        sample["c-Comment"] = False
                        for tag in ["Einstellung", "Interpretation", "Meta"]:
                            if tag in tags:
                                sample["c-" + tag] = True
                                sample["c-Comment"] = True
                            else:
                                sample["c-" + tag] = False
                        sample["c-Nichtfiktional"] = False
                        sample["c-Nichtfiktional+mK"] = False
                        if "Nichtfiktional" in tags:
                            sample["c-Nichtfiktional"] = True
                            sample["c-Nichtfiktional+mK"] = True
                        elif "Nichtfiktional+mK" in tags:
                            sample["c-Nichtfiktional+mK"] = True
                        count_attr = {val : [np.nan for i in range(6)] for val in ["Figur", "Erzählinstanz", "Verdacht Autor", "Werkexterne Instanz"]}
                        attr = False
                        for a, annotator in enumerate(["A1", "A2", "A3", "A4", "A5", "A6"]):
                            for attribution in clause._.annotations[annotator].get_annotations(tags=["Attribution"], tagset="Attribution IV"):
                                props = attribution.property_values
                                if "Attribution" in props:
                                    vals = props["Attribution"]
                                    for val in count_attr:
                                        count_attr[val][a] = int(val in vals)
                                attr = True
                        for val in count_attr:
                            if attr:
                                count_attr[val] = np.mean(count_attr[val])
                                sample["c-" + val] = (count_attr[val] >= 0.5)
                            sample["c-" + val + "*"] = (val in clause._.attribution)
                        sample = {key : str(sample[key]) for key in sample}
                        x = Sample(sample)
                        X.append(x)

    fs_ = set([f for x in X for f in x.fs])
    fs = [f for f in fs_ if f.count("&") == 0]
    print(len(fs))
    results = []
    for f in tqdm.tqdm(fs):
        results.append(all_measures(X, "GI", f))
    results = filter(lambda res: res[3] is not None, results)
    results = filter(lambda res: res[3] >= 0.1, results)
    #results = filter(lambda res: res[5] == "*", results)
    results = sorted(results, key=lambda res: res[3], reverse=True)
    for _, f, n, v, x2, significance, p, direction in results:
        n_p = 1.0*n/len(X)
        X_f = [x for x in X if x.check(f)]
        try:
            eps = count(X_f, "GI", f)-calc(X_f, "GI", f)
        except ZeroDivisionError:
            continue
        polarity = ""
        if eps < 0:
            polarity = "!"
            p = P_D(X_f, "GI", not_(f))
        if p < 0:
            direction = polarity + "f ==> y"
        else:
            direction = "y ==> " + polarity + "f"
        print(format_row(["GI", f, n, n_p, v, x2, significance, eps, p, direction]))