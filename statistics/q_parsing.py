QUANTIFIER_LEMMAS = {
    "all" : "ALL",
    "allabendlich" : "ALL", # new
    "alle": "ALL",
    "allemal" : "ALL", # new
    "allezeit" : "ALL", # new
    "ausschließlich" : "ALL", # new
    "beständig" : "ALL", # new
    "ewigeinigen" : "ALL", # new
    "ganz" : "ALL", # new
    "ganze" : "ALL", # new
    "gänzlich" : "ALL",
    "immer" : "ALL",
    "immerzu" : "ALL",
    "jed" : "ALL",
    "jedermann" : "ALL", # new
    "jederzeit" : "ALL", # new
    "jedesmal" : "ALL", # new
    "jedwed" : "ALL", # new
    "sämmtlichen" : "ALL", # new
    "sämtlich": "ALL",
    "stets" : "ALL",
    "überall" : "ALL",
    "ueberall" : "ALL", # new

    "mehrheitlich" : "MEIST",
    "meist" : "MEIST",
    "meistens" : "MEIST",
    "meistenteils" : "MEIST",
    "überwiegend" : "MEIST",
    "vorwiegend" : "MEIST",

    "allerhand" : "DIV", # new
    "allerlei" : "DIV",
    "einig" : "DIV",
    "einige" : "DIV", # new
    "einigen" : "DIV", # new
    "einiges" : "DIV",
    "etlich" : "DIV",
    "manch" : "DIV",
    "mancherlei" : "DIV",
    "mehrer" : "DIV",
    "mehrern" : "DIV", # new
    "viel" : "DIV", 
    "vielerlei" : "DIV", 
    "wenig" : "DIV",
    "zahlreich" : "DIV",
    "zahlreiche" : "DIV", # new

    "bisweilen" : "DIV",
    "einigemal" : "DIV",
    "etlichemal" : "DIV",
    "gängig" : "DIV",
    "gebräuchlich" : "DIV",
    "geläufig" : "DIV",
    "gern" : "DIV", # new
    "häufig" : "DIV",
    "herkömmlich" : "DIV",
    "mal" : "DIV", # new
    "manchmal" : "DIV",
    "mitunter" : "DIV",
    "oft" : "DIV",
    "öfter" : "DIV",
    "öftermals" : "DIV", # new
    "öfters" : "DIV",
    "paarmal" : "DIV", # new
    "selten" : "DIV",
    "ständig" : "DIV",
    "teils" : "DIV", # new
    "unzähligemal" : "DIV", # new
    "zeitweise" : "DIV",
    "zuweilen" : "DIV",
    "zuzeiten" : "DIV", # new

    "allgemein" : "DIV",
    "alltäglich" : "DIV",
    "gelegentlich" : "DIV", # new
    "gemeinhin" : "DIV",
    "gemeiniglich" : "DIV",
    "generell" : "DIV",
    "gewöhnlich" : "DIV",
    "gewöhnlicherweise" : "DIV",
    "normalerweise" : "DIV",
    "regelmäßig" : "DIV",
    "typischerweise" : "DIV",
    "üblich" : "DIV",
    "üblicherweise" : "DIV",

    "kein" : "NEG",
    "keineswegs" : "NEG",
    "keins" : "NEG", # new
    "nichts" : "NEG",
    "nie" : "NEG",
    "niemals" : "NEG",
    "niemand" : "NEG",
    "nimmer" : "NEG", # new
    "nirgends" : "NEG",
    "nirgendwo" : "NEG",

    "existieren" : "EXIST",
    "gabs" : "EXIST", # new
    "geben" : "EXIST",
    "giebt" : "EXIST", # new
    "sein" : "EXIST", # new
}

NEGATOR_LEMMAS = set([
    "nein", # new
    "nich", # new
    "nicht",
    "weder", # new
])


def parse(tokens, tag, annotated_referents):
    """Q-parsing algorithm.

    Args:
        tokens (list of `Token`): The tokens of a passage.
        tag (str): The tag of the annotation.
        annotated_referents (list of `Token`): All key referents in the document.
    
    Returns:
        `Span`: Clause.
        `Token`: Key referent.
        `Token`: Q-word.
        `Token`: Negator.
    
    """
    q_words = [token for token in tokens if token.lemma_.lower() in QUANTIFIER_LEMMAS and check_referent(token)]
    if tag in ["ALL", "DIV", "EXIST", "MEIST"] or (tag in ["NEG", "NONE"] and len(q_words) > 0):
        if tag in ["ALL", "DIV", "EXIST", "MEIST", "NEG"]:
            q_word = first_matching_q_word(q_words, tag, tokens)
        else:
            q_word = q_words[0]
        referent = get_referent(q_word)
    else:
        if tag in ["BARE", "NEG"]:
            referent = first_referent(tokens, annotated_referents)
        else:
            referent = first_plural_NP(tokens)
        q_word = None
    negators = [token for token in tokens if token.lemma_.lower() in NEGATOR_LEMMAS]
    if tag in ["NEG", "NONE"] and len(negators) > 0:
        if referent is not None:
            negator = sorted(negators, key=lambda negator: dep_distance(negator, referent, list(negator.doc.sents)))[0]
        elif q_word is not None:
            negator = sorted(negators, key=lambda negator: dep_distance(negator, q_word, list(negator.doc.sents)))[0]
        else:
            negator = negators[0]
    else:
        negator = None
    if tag == "NEG" and (q_word is None or QUANTIFIER_LEMMAS[q_word.lemma_.lower()] != "NEG") and negator is None:
        raise Exception("No negation in " + str(tokens))
    if referent is not None:
        clause = referent._.clause
    elif q_word is not None:
        clause = q_word._.clause
    elif negator is not None:
        clause = negator._.clause
    else:
        clause = None
    return clause, referent, q_word, negator


def first_matching_q_word(q_words, tag, tokens=None):
    """Return the first matching Q-word from a list.

    Args:
        q_words (list of `Token`): List of Q-words.
        tag (str): The tag of the annotation.
    
    Returns:
        `Token`: The first Q-word in the list that matches the tag.
    
    """
    try:
        # sometimes "beständig" is annotated as DIV ...
        return [token for token in q_words if QUANTIFIER_LEMMAS[token.lemma_.lower()] == tag or (token.lemma_.lower() == "beständig" and tag == "DIV")][0]
    except IndexError:
        if tag == "NEG":
            return q_words[0]
        else:
            raise Exception("No matching Q-word for tag " + tag + " in " + str(tokens))


def check_referent(token):
    """Check whether a referent can be returned for a token (Q-word).

    Args:
        token (`Token`): A token (Q-word).
    
    Returns:
        boolean: Wether `get_referent` returns something (True) or throws an exception (False).
    
    """
    try:
        get_referent(token)
        return True
    except:
        return False


def get_referent(token):
    """Return the referent of a token (Q-word).

    Args:
        token (`Token`): A token (Q-word).
    
    Returns:
        `Token`: The parent or child of the token, depending on the parse.
    
    """
    if token is None:
        return None
    if token.pos_ in ["DET", "ADJ", "ADV"]:
        return token.head
    elif token.pos_ == "PRON":
        for child in token.children:
            if child.dep_.split(":")[0] == "nmod":
                return child
        return None
    elif token.pos_ in ["VERB", "AUX"]:
        for child in token.children:
            if child.dep_.split(":")[0] == "obj":
                return child
        for child in token.children:
            if child.dep_.split(":")[0] == "nsubj":
                return child
    raise Exception("No referent for " + str(((token, token.pos_, token.dep_), (token.head, token.head.pos_, token.head.dep_), [(child, child.pos_, child.dep_) for child in token.children])))


def first_referent(tokens, referents):
    """Return the first token from a list that is a key referent.

    Args:
        tokens (list of `Token`): List of tokens (e.g. the tokens of a passage).
        referents (list of `Tokens`): All key referents in a document.
    
    Returns:
        `Token`: The first token that is a key referent.
    
    """
    try:
        return [token for token in tokens if token in referents][0]
    except IndexError:
        raise Exception("No referent in " + str(tokens))


def first_plural_NP(tokens):
    """Return the first plural NP from a list of tokens.

    Args:
        tokens (list of `Token`): List of tokens (e.g. the tokens of a passage).
    
    Returns:
        `Token`: The first NP head with the highest plurality score.
    
    """
    NPs = [token for token in tokens if is_NP(token)]
    NPs = sorted(NPs, key=lambda NP: -plurality_score(NP))
    try:
        return NPs[0]
    except IndexError:
        return None


def is_NP(token):
    """Checks whether a token has a nominal UPOS tag or is governed by a nominal UD relation.

    Args:
        token (`Token`): A token.
    
    Returns:
        boolean: True iff the token is governed by a nominal UD relation.
    
    """
    np_heads = ["NOUN", "PROPN", "PRON"]
    np_deps = ["nsubj", "obj", "iobj", "obl", "nmod"]
    dep = token.dep_.split(":")[0]
    if token.pos_ in np_heads or dep in np_deps or (dep == "conj" and token.head.dep_.split(":")[0] in np_deps):
        return True
    return False


def plurality_score(token):
    """A score for the plurality of a token.

    Args:
        token (`Token`): A token.
    
    Returns:
        int: Plurality score.
            4: plural NP
            3: ambiguous NP
            2: singular NP
            1: "es"
            0: [no number feature]
            -1: [other than singular/plural]
    
    """
    morph = token._.morph
    if len(morph.numerus_) == 0:
        return 0
    elif token.text.lower() == "es":
        return 1
    elif morph.numerus == "sing":
        return 2
    elif morph.numerus is None:
        return 3
    elif morph.numerus == "plu":
        return 4
    return -1


def phrase_type(token):
    """Return the phrase type for a token (phrase head).

    Args:
        token (`Token`): The token (phrase head).
    
    Returns:
        str: "NP" or "VP".
    
    """
    if token.pos_ in ["VERB", "AUX", "ADV", "PART"]:
        return "VP"
    elif token.pos_ in ["NOUN", "PROPN", "PRON", "ADJ", "DET", "NUM", "ADP"]:
        return "NP"
    elif token.pos_ in ["SPACE", "PUNCT", "X", "SYM", "CCONJ", "SCONJ", "INTJ"]:
        return None
    return None


def word_order(clause):
    """Return the word order of a clause.

    Args:
        clause (`Span`): A clause.
    
    Returns:
        str: "V1", "V2", ... (verb position counted from start)
        str: "X1", "X2", ... (verb position counted from end)
    
    """
    if clause._.form is None or len(clause._.form.verbs) == 0:
        return None, None
    verb = clause._.form.verbs[-1]
    constituents = sorted([token for token in list(clause.root.children)+[clause.root] if not (token.is_punct or token.is_space or token.dep_.split(":")[0] in ["cc", "conj", "parataxis", "list", "discourse"])], key=lambda token: token.i)
    if verb not in constituents:
        i = 0
        while len(constituents) > i and not (verb.i < constituents[i].i):
            i += 1
        constituents.insert(i, verb)
    position = constituents.index(verb)
    return "V" + str(position+1), "X" + str(len(constituents)-position)


def dep_distance(token1, token2, sents, root1=False):
    """Compute the minimal dependency distance between two tokens.

    Args:
        token1 (`Token`): A token.
        token2 (`Token`): A token.
        sents (list of `Span`): List of sentences.
        root1 (boolean): Can be set to true if it is guaranteed that `token1` is an ancestor of `token2`.
    
    Returns:
        int: Minimal dependency distance.
    
    """
    if token1.sent != token2.sent:
        # if tokens are from different sentences,
        # return the distance between the sentences plus the distance of each token to its root
        return abs(sents.index(token1.sent)-sents.index(token2.sent))+root_distance(token1)+root_distance(token2)
    
    if root1:
        # if token1 is ancestor of token2,
        # the lowest common ancestor is token1
        node = token1
    else:
        # otherwise,
        # compute lowest common ancestor
        token_dists = []
        for token in [token1, token2]:
            dist = 0
            current = token
            while current.head != current:
                current = current.head
                dist += 1
            token_dists.append((token, dist))
        token_dists = sorted(token_dists, key=lambda token_dist: token_dists[1])
        token1 = token_dists[0][0]
        token2 = token_dists[1][0]
        node = token1
        while not (node == token2 or node.is_ancestor(token2)):
            node = node.head
    
    # add the distances from each token
    # to the lowest common ancestor
    dist = 0
    for token in [token1, token2]:
        current = token
        while current != node:
            current = current.head
            dist += 1
    
    return dist


def root_distance(token):
    """Compute the minimal dependency distance from a token to the root node.

    Args:
        token (`Token`): A token.
    
    Returns:
        int: Minimal dependency distance.
    
    """
    return dep_distance(token.sent.root, token, [token.sent], True)